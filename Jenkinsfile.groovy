#!groovy
// Declarative //
pipeline {
    agent any
    environment {
//        SUCCESS_EMAIL_RECIPIENTS = '''hrico@rccl.com,
//                dgarzon@rccl.com,
//                rcampbell@rccl.com,
//                jmantovani@rccl.com,
//                ogarcia@rccl.com,
//                jostrowski@rccl.com,
//                davidamartinez@rccl.com,
//                kalvarado@rccl.com,
//                aredy@rccl.com,
//                svazquez@rccl.com,
//                drepik@rccl.com,
//                pcarabeo-nieva@rccl.com,
//                ehuacuz@rccl.com,
//                dhoyos@rccl.com,
//                dmoreno@rccl.com,
//                bpiste@rccl.com,
//                aquintero@rccl.com,
//                jesusrivas@rccl.com,
//                a.vargas@globant.com,
//                gbarroso@rccl.com,
//                lgonda@rccl.com'''
        SUCCESS_EMAIL_RECIPIENTS = 'hrico@rccl.com'
        FAIL_EMAIL_RECIPIENTS = 'hrico@rccl.com'
    }
    options {
        disableConcurrentBuilds()
    }
    parameters {
        choice(name: 'AEM_ENVIRONMENT', choices: '\ndev\ntest\nstage\npreprod', description: 'What environment?')
    }
    stages {
        stage('Build') {
            steps {
                script {
                    env.branchType = getBranchType(env.BRANCH_NAME)
                    echo "Environment selected:" + env.AEM_ENVIRONMENT
                    env.branchDeploymentEnvironment = env.AEM_ENVIRONMENT == '' || env.AEM_ENVIRONMENT == null ? getBranchDeploymentEnvironment(env.branchType) : env.AEM_ENVIRONMENT
                    pom = readMavenPom file: 'pom.xml'
                    env.pomVersion = pom.properties.'global.version'
                    env.shouldDeploy = (env.branchType == 'dev' || env.branchType == 'release' || env.branchType == 'hotfix' || (env.branchType == 'feature' || env.branchType == 'bugfix') && env.SHOULD_DEPLOY != null)
                    env.hasDispatcherChanges = hasDispatcherChanges()
                    echo "has dispatcher changes: " + env.hasDispatcherChanges
                    currentBuild.displayName = env.BRANCH_NAME + "-v" + env.pomVersion + " #" + env.BUILD_NUMBER
                }

                echo "branch type: " + env.branchType
                echo "branch environment: " + env.branchDeploymentEnvironment
                echo "pom version: " + env.pomVersion
                echo "should deploy: " + env.shouldDeploy
                println "building..."

//                withMaven(maven: 'mvn311'){
//                    sh 'mvn clean install -Ppublic'
//                }


            }
        }
        stage('Upload package to Nexus') {
            steps {
                echo 'uploading package to Nexus artifact repository'
                //nexusArtifactUploader artifacts: [[artifactId: 'royal.ui.apps', file: 'royal.ui.apps', classifier: 'debug', file: 'ui.apps/target/royal.ui.apps-1.8.0-SNAPSHOT.zip', type: 'zip']], credentialsId: 'nexus-royal', groupId: 'com.royalcaribbean.aem', nexusUrl: 'nexus.rccl.com:8081', nexusVersion: 'nexus2', protocol: 'http', repository: 'royal-snapshot', version: '1.8.0-SNAPSHOT'
            }
        }
        stage('Install') {
            parallel{
                stage('Install Author'){
                    steps {
                        withCredentials([
                                [$class: 'UsernamePasswordMultiBinding', credentialsId: 'aem_' + env.branchDeploymentEnvironment, usernameVariable: 'AEM_USER', passwordVariable: 'AEM_PASS'],
                            ]){

//                            sh 'curl -u "${AEM_USER}:${AEM_PASS}" -F file=@"ui.apps/target/royal.ui.apps-${pomVersion}.zip" -F force=true -F install=true http://royal-${branchDeploymentEnvironment}-author1.adobecqms.net:4502/crx/packmgr/service.jsp'
//                            sh 'curl -u "${AEM_USER}:${AEM_PASS}" -F action=install -F bundlestartlevel=20 -F bundlefile=@"core/target/royal.core-${pomVersion}.jar" http://royal-${branchDeploymentEnvironment}-author1.adobecqms.net:4502/system/console/bundles'

                            echo "package installed in Authors"
                        }
                    }
                }
                stage('Install Publishers'){
                    steps {
                        withCredentials([
                                [$class: 'UsernamePasswordMultiBinding', credentialsId: 'aem_' + env.branchDeploymentEnvironment, usernameVariable: 'AEM_USER', passwordVariable: 'AEM_PASS'],
                            ]){

                            script{
                                def publishers = getNumberOfDispatchers(env.branchDeploymentEnvironment)
//                                for (int i = 1; i <= publishers; i++) {
//                                    try{
//                                        sh 'curl -u "${AEM_USER}:${AEM_PASS}" -F file=@"ui.apps/target/royal.ui.apps-${pomVersion}.zip" -F force=true -F install=true http://royal-${branchDeploymentEnvironment}-publish' + i + '.adobecqms.net:4503/crx/packmgr/service.jsp'
//                                        sh 'curl -u "${AEM_USER}:${AEM_PASS}" -F action=install -F bundlestartlevel=20 -F bundlefile=@"core/target/royal.core-${pomVersion}.jar" http://royal-${branchDeploymentEnvironment}-publish' + i + '.adobecqms.net:4503/system/console/bundles'
//                                    }catch(Exception e){
//                                        echo 'Error trying to install in publish' + i;
//                                    }
//                                }
                            }

                            echo "package installed in Publishers"
                        }
                    }
                }
            }
        }
        stage('Deploy Dispatcher Rules'){
            when {
                expression { return env.hasDispatcherChanges == 'true' }
            }
            steps {
                script{
                    def dispatchers = getNumberOfDispatchers(env.branchDeploymentEnvironment)
                    def environmentKey = getKeyFromEnvironment(env.branchDeploymentEnvironment)

                    dir ('tools/dispatcher') {
                        sh './generate.sh ' + environmentKey
                        for (int i = 1; i <= dispatchers; i++) {
                            sh 'scp -i ~/.ssh/royal-aem-dispatcher.rsa target/' + environmentKey + '/vhost.d/royal/* royal-admin@royal-${branchDeploymentEnvironment}-dispatcher' + i+ '.adobecqms.net:/etc/httpd/vhost.d/royal'
                            sh 'scp -i ~/.ssh/royal-aem-dispatcher.rsa target/' + environmentKey + '/vhost.d/006_author_aem_rccl_com.conf royal-admin@royal-${branchDeploymentEnvironment}-dispatcher' + i+ '.adobecqms.net:/etc/httpd/vhost.d'
                            sh 'scp -i ~/.ssh/royal-aem-dispatcher.rsa target/' + environmentKey + '/vhost.d/011_www_royalcaribbean_com.conf royal-admin@royal-${branchDeploymentEnvironment}-dispatcher' + i+ '.adobecqms.net:/etc/httpd/vhost.d'
                            sh 'ssh -i ~/.ssh/royal-aem-dispatcher.rsa -t royal-admin@royal-${branchDeploymentEnvironment}-dispatcher' + i+ '.adobecqms.net "sudo apachectl restart"'
                        }
                    }

                }

                echo "Rules installed"
            }
        }

        stage('Clear Dispatcher Cache') {
            when {
                expression { env.shouldDeploy }
            }
            steps{
//                build job: 'AEM-clear-cache-' + env.branchDeploymentEnvironment
                println "Dispatcher and Akamai cache cleared"
            }
        }
        stage('Test') {
            steps {
                dir ('tools/bdd') {
                    sh 'npx -p node@8 npm -version'
                    sh 'node --version'
                    sh 'npm install'
                    sh 'webdriver-manager start'
                    sh 'npm run pretest'
                    sh 'npm run build'
                    sh 'protractor config/conf.cucumber.js --baseUrl="https://www.stage2.royalcaribbean.com/"'
                }
                println "Testing done"
            }
        }
        stage('Sanity check') {
            steps {
                dir ('tools/bdd') {
                    sh 'npm install'
                    sh 'node_modules/phantomjs-prebuilt/bin/phantomjs --webdriver=4444 &'
                    sleep time: 5, unit: 'SECONDS'
                    sh 'node_modules/grunt-cli/bin/grunt'
                }
                println "Sanity check successful"
            }
        }
        stage('Update JIRA tickets'){

            when {
                expression { env.shouldDeploy }
            }
            steps{
                script{
                    def jiraTickets = getJiraTickets()
                    echo 'updating jira tickets:' + jiraTickets

                    jiraTickets.each{ ticketId ->

//                        if(ticketId.startsWith('AEM') && branchType != 'feature' && branchType != 'bugfix' && branchType != 'hotfix'){
//                            def transitionInput =
//                            [
//                                transition: [
//                                    id: '351'
//                                ]
//                            ]
//
//                            jiraTransitionIssue idOrKey: ticketId, input: transitionInput, site: 'Royal'
//                        }
                        def issue = jiraGetIssue site: "Royal", idOrKey: ticketId
                        println 'labels ' + issue.data.fields.labels
                        def labels = [] as String[]
                        labels += issue.data.fields.labels.toArray(new String[0])
                        def labelList = labels.toList()
                        labelList.add('deployed_in_' + env.branchDeploymentEnvironment)
                        def jiraIssue = [fields: [labels: labelList]]
                        jiraEditIssue site: "Royal", idOrKey: ticketId, issue: jiraIssue
                        jiraAddComment site: "Royal", idOrKey: ticketId, comment: "(/) A package has been deployed for this ticket in " + env.branchDeploymentEnvironment + " ([#" + BUILD_NUMBER + "|" + RUN_DISPLAY_URL + "])"
                        if(issue.data.fields.customfield_15400 != null && issue.data.fields.customfield_15400 != ''){
                            def demoPackageURL = issue.data.fields.customfield_15400.replace('/crx/packmgr/index.jsp#','')
                            println 'demo package: ' + demoPackageURL
                            def demoPackageRelativeURL = demoPackageURL.substring(demoPackageURL.indexOf('.com/') + 4, demoPackageURL.length())
                            println 'demo package relative URL ' + demoPackageRelativeURL
                            def demoPackageEnvironment = getEnvironmentFromUrl(demoPackageURL)
                            println 'environment from url: ' + demoPackageEnvironment
                            withCredentials([
                                    [$class: 'UsernamePasswordMultiBinding', credentialsId: 'aem_' + demoPackageEnvironment, usernameVariable: 'AEM_USER', passwordVariable: 'AEM_PASS'],
                                ]){
                                sh 'curl -u "${AEM_USER}:${AEM_PASS}" ' + demoPackageURL + ' > demopackage.zip'
                            }
                            withCredentials([
                                    [$class: 'UsernamePasswordMultiBinding', credentialsId: 'aem_' + env.branchDeploymentEnvironment, usernameVariable: 'AEM_USER', passwordVariable: 'AEM_PASS'],
                                ]){

                                sh 'curl -u "${AEM_USER}:${AEM_PASS}" -F file=@"demopackage.zip" -F force=true -F install=true http://royal-${branchDeploymentEnvironment}-author1.adobecqms.net:4502/crx/packmgr/service.jsp'
                                echo "Demo package installed"
                                sh 'curl -u "${AEM_USER}:${AEM_PASS}" -F cmd=replicate "http://royal-${branchDeploymentEnvironment}-author1.adobecqms.net:4502/crx/packmgr/service/script.html' + demoPackageRelativeURL + '"'
                                echo "Demo package replicated"
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            //            junit 'build/reports/**/*.xml'
            archive 'build/libs/**/*.jar'
        }
        success {
            script{
                if (env.branchType == 'dev' || env.branchType == 'release'){
                    sendEmail("SUCCESS", SUCCESS_EMAIL_RECIPIENTS)
                }else{
                    sendEmail("SUCCESS", FAIL_EMAIL_RECIPIENTS)
                }
            }
            echo 'SUCCESS'
        }
        failure {
            echo 'FAIL'
            sendEmail("FAIL", FAIL_EMAIL_RECIPIENTS)
        }
    }
}

@NonCPS
def getChangeString() {
    def changeString = ""

    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            echo "entry " + entry
            truncated_msg = entry.msg.replaceAll(/[A-Z]+-[0-9]+/){ m -> "<a href='https://rccl-idd.atlassian.net/browse/${m}'>${m}</a>" }
            changeString += "<li>${truncated_msg} [${entry.author}] [<a href='https://bitbucket.org/aemdevelopmentteam/rccl/commits/${entry.commitId}'>commit</a>]</li>"
        }
    }

    if (!changeString) {
        changeString = " - No new changes"
    }
    return changeString
}

def sendEmail(status, to) {
    emailext (
        mimeType: 'text/html',
        to: "$to",
        subject: "Build $BUILD_NUMBER - " + status + " ($JOB_NAME)",
        body: '''
             <b>AEM package version: ''' + env.pomVersion + ''' (build #$BUILD_NUMBER) has been installed in ''' + env.branchDeploymentEnvironment + ''' </b><br/>
             <b>Branch: ''' + env.BRANCH_NAME +'''</b><br/>
             <b>Result: ''' + status +'''</b><br/><br/>
             Changes: <br/>''' + getChangeString() + '''<br/><br/>
             Check Blue Ocean report: <a href='$RUN_DISPLAY_URL'>here</a><br/>
             Check Sonar report (<i>new*</i>): <a href='http://sonar-30589353.us-east-1.elb.amazonaws.com/dashboard?id=com.royalcaribbean.aem:royal'>here</a><br/>
             Check Complete list of changes: <a href='$RUN_CHANGES_DISPLAY_URL'>here</a><br/>
             Check console output: <a href='$BUILD_URL/console'>here</a><br/>
             A comment and a label have been created for related tickets in JIRA.
          ''',
        attachLog: false)
}

@NonCPS
def getJiraTickets() {
    Set jiraTickets = []

    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            def ticketId = entry.msg =~ /[A-Z]+-[0-9]+/
            if(ticketId.asBoolean()){
                def tid = ticketId.getAt(0)
                if(!jiraTickets.contains(tid)){
                    jiraTickets.add(tid);
                }
            }
        }
    }
    return jiraTickets
}

def getBranchType(String branchName) {
    //Must be specified according to <flowInitContext> configuration of jgitflow-maven-plugin in pom.xml
    def devPattern = ".*dev"
    def releasePattern = ".*release/.*"
    def featurePattern = ".*feature/.*"
    def bugfixPattern = ".*bugfix/.*"
    def hotfixPattern = ".*hotfix/.*"
    def masterPattern = ".*master"
    if (branchName =~ devPattern) {
        return "dev"
    } else if (branchName =~ releasePattern) {
        return "release"
    } else if (branchName =~ masterPattern) {
        return "master"
    } else if (branchName =~ bugfixPattern) {
        return "bugfix"
    } else if (branchName =~ featurePattern) {
        return "feature"
    } else if (branchName =~ hotfixPattern) {
        return "hotfix"
    } else {
        return null;
    }
}

def getBranchDeploymentEnvironment(String branchType) {
    if (branchType == "dev") {
        return "stage"
    } else if (branchType == "release" || branchType == "hotfix") {
        return "preprod"
    } else if (branchType == "feature" || branchType == "bugfix") {
        return "test"
    } else if (branchType == "master") {
        return "prod"
    } else {
        return null;
    }
}

def getEnvironmentFromUrl(url) {
    def stagePattern = ".*stg.*"
    def testPattern = ".*tst.*"
    def preprodPattern = ".*new.*"
    def devPattern = ".*dev.*"
    if (url =~ stagePattern) {
        return "stage"
    } else if (url =~ testPattern) {
        return "test"
    } else if (url =~ devPattern) {
        return "dev"
    } else if (url =~ preprodPattern) {
        return "preprod"
    } else {
        return null;
    }
}

def getNumberOfDispatchers(environment) {
    if(environment == 'dev'){
        return 1
    }else if(environment == 'test'){
        return 2
    }else if(environment == 'stage'){
        return 2
    }else if(environment == 'preprod'){
        return 3
    }
}

def getKeyFromEnvironment(environment) {
    if(environment == 'dev'){
        return 'dev'
    }else if(environment == 'test'){
        return 'tst'
    }else if(environment == 'stage'){
        return 'stg'
    }else if(environment == 'preprod'){
        return 'new'
    }
}

@NonCPS
def hasDispatcherChanges() {

    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            def files = new ArrayList(entry.affectedFiles)
            for (int k = 0; k < files.size(); k++) {
                def file = files[k]
                echo "File changed ${file.path}"
                if(file.path.startsWith("tools/dispatcher")){
                    return true
                }
            }
        }
    }
    return false
}

