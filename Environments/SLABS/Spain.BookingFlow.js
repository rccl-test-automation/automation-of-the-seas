var helper = require('../../helper.js');
var CommonElements = require('../common/common.po.js');
var SlabsElements = require('./SLABS.po.js');

var commonElements = new CommonElements();
var slabsElements = new SlabsElements();

describe('A user booking a cruise', function() {

  beforeAll(function() {
    browser.ignoreSynchronization = true;
    //browser.get(browser.params.url);
    browser.get('https://www.royalcaribbean.es/?wuc=ESP');
  });

  // afterEach(function(){
  //   browser.manage().deleteAllCookies();
  // });

  // Entering AEM
  it('should select Search on the Homepage', function() {
    commonElements.searchButton.click();
  });

  //Cruise Search Selection
  it( 'should check the url and enter the correct flow' , function() {
    browser.getCurrentUrl().then(function(url) {
        if ( url === 'https://secure.royalcaribbean.com/cruises/' ) {
          slabsElements.newCruiseSearch();
        } else {
          slabsElements.oldCruisesearch();
        }
    });
  });

    //Starts the interior booking Flow
    it('should start the bookingflow', function() {
      slabsElements.interiorFlow();
    });

    //Enter guest info
    it('should enter the guest info', function() {
      commonElements.guestInfo();
    });

    //restart the flow
    it('should restart the flow', function() {
      commonElements.restartFlow();
      slabsElements.oldCruisesearch();
    });
    //
    // //Starts the oceanview booking Flow
    // it('should select an oceanview room and continue thru the booking flow', function() {
    //   spaElements.oceanviewFlow();
    // });
    //
    // //Enter guest info
    // it('should enter the guest info', function() {
    //   commonElements.guestInfo();
    // });
    //
    // //restart the flow
    // it('should restart the flow', function() {
    //   commonElements.restartFlow();
    //   spaElements.newCruiseSearch();
    // });
    //
    // //Starts the Balcony booking Flow
    // it('should select an Balcony room and continue thru the booking flow', function() {
    //   spaElements.balconyFlow();
    // });
    //
    // //Enter guest info
    // it('should enter the guest info', function() {
    //   commonElements.guestInfo();
    // });
    //
    // //restart the flow
    // it('should restart the flow', function() {
    //   commonElements.restartFlow();
    //   spaElements.newCruiseSearch();
    // });
    //
    // //Starts the suite booking Flow
    // it('should select a suite and continue thru the booking flow', function() {
    //   spaElements.suiteFlow();
    // });
    //
    // //Enter guest info
    // it('should enter the guest info', function() {
    //   commonElements.guestInfo();
    // });
    //
    // //restart the flow
    // it('should restart the flow', function() {
    //   commonElements.restartFlow();
    // });

  });
