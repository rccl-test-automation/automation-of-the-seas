var helper = require('../../helper.js');
var CommonElements = require('../common/common.po.js');
var SpaElements = require('../SPA/SPA.po.js');
var SlabsElements = require('./SLABS.po.js');

var commonElements = new CommonElements();
var spaElements = new SpaElements();
var slabsElements = new SlabsElements();

describe('A user booking a cruise', function() {

  beforeAll(function() {
    browser.ignoreSynchronization = true;
    browser.get(browser.params.url);
  });
  afterAll(function(){
    browser.manage().deleteAllCookies();
  });

  // Entering AEM
  it('should select Brazil and then Search on the Homepage', function() {
    commonElements.brazilcountrySelection();
    commonElements.searchButton.click();
  });

  //Cruise Search Selection
  it( 'should select Allure and click book now' , function() {
    slabsElements.oldCruisesearchBrazil();
  });

  //Starts the interior booking Flow
  it('should go thru the booking flow', function() {
    slabsElements.interiorFlow();
  });
  //
  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonElements.guestInfo();
  // });
  //
  // //restart the flow
  // it('should restart the flow', function() {
  //   commonElements.restartFlow();
  //   spaElements.newCruiseSearchAUS();
  // });
  //
  // //Starts the oceanview booking Flow
  // it('should select an oceanview room and continue thru the booking flow', function() {
  //   spaElements.oceanviewFlow();
  // });
  //
  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonElements.guestInfo();
  // });
  //
  // //restart the flow
  // it('should restart the flow', function() {
  //   commonElements.restartFlow();
  //   spaElements.newCruiseSearchAUS();
  // });
  //
  // //Starts the suite booking Flow
  // it('should select an oceanview room and continue thru the booking flow', function() {
  //   spaElements.suiteFlow();
  // });
  //
  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonElements.guestInfo();
  // });
  //
  // //restart the flow
  // it('should restart the flow', function() {
  //   commonElements.restartFlow();
  // });

});
