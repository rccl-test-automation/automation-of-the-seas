var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SlabsFunctions = require('../common/SLABS.functions.js');
var SlabsElements = require('../common/SLABS.objects.js');

var slabsFunctions = new SlabsFunctions();
var slabsElements = new SlabsElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();

describe('A user booking a cruise', function() {

  beforeAll(function() {
    browser.get(browser.params.url);
    browser.ignoreSynchronization = true;
  });
  afterAll(function(){
    browser.manage().deleteAllCookies();
  });

  // Entering AEM
  it('should select Australia and then Search on the Homepage', function() {
    slabsFunctions.countrySelectionAUS();
    helper.waitUntilReady(commonElements.searchButton);
    commonElements.searchButton.click();
  });

  //Cruise Search Selection
  it( 'should pick a cruise from cruise search' , function() {
    slabsFunctions.newCruiseSearchAUS();
  });

  //Starts the interior booking Flow
  it('should select an interior room and continue thru the booking flow', function() {
    slabsElements.continueStateroom.click();
    slabsFunctions.interiorFlowAUS();
  });

  //Enter guest info
  it('should enter the guest info', function() {
    commonFunctions.guestInfoAUS();
  });

  //restart the flow
  it('should restart the flow', function() {
    commonFunctions.restartFlow();
    slabsFunctions.newCruiseSearchAUS();
  });

  // //Starts the oceanview booking Flow
  // it('should select an oceanview room and continue thru the booking flow', function() {
  //   spaElements.oceanviewFlowAUS();
  // });
  //
  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonElements.guestInfoAUS();
  // });
  //
  // //restart the flow
  // it('should restart the flow', function() {
  //   commonElements.restartFlow();
  //   spaElements.newCruiseSearchAUS();
  // });
  //
  // //Starts the suite booking Flow
  // it('should select an suite room and continue thru the booking flow', function() {
  //   spaElements.suiteFlowAUS();
  // });
  //
  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonElements.guestInfoAUS();
  // });
  //
  // //restart the flow
  // it('should restart the flow', function() {
  //   commonElements.restartFlow();
  // });

});
