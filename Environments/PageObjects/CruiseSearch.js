var helper = require('../../helper.js');
var a = Math.floor(Math.random() * 10);


var CruiseSearch = function(){
  //objects
  this.bookNow =element(by.css('[data-selector="search-cards-'+a+'"]'));
  this.bookTour =element(by.css('div[packagecode*="RD09C87B-2018-05-30"'));
  this.desFilter =element(by.css('[data-selector="search-any-destination"]'));
  this.alaskaFilter =element(by.css('[data-selector="search-destination-1"]'));
  this.applyButton =element(by.css('[data-selector="search-apply-button"]'));
  this.selectDate =element(by.css('[data-selector="search-panel-date1"]'));
  this.dateFilter =element(by.id('md-button-toggle-21-input'));
  this.productView =element(by.id('p'));

  // Cruise Search
    this.bookRandomCruise = function (){
      helper.waitUntilReady(this.bookNow);
      this.bookNow.click();
      helper.waitUntilReady(this.selectDate);
      browser.sleep(2000);
      this.selectDate.click();
  }
  // CruiseTour
    this.cruiseTour = function (){
      browser.sleep(2000);
      helper.waitElementToBeClickable(this.desFilter);
      this.desFilter.click();
      browser.sleep(2000);
      helper.waitUntilReady(this.alaskaFilter);
      this.alaskaFilter.click();
      browser.sleep(2000);
      helper.waitUntilReady(this.applyButton);
      this.applyButton.click();
      browser.sleep(2000);
      helper.waitUntilReady(this.bookTour);
      this.bookTour.click();
      helper.waitUntilReady(this.selectDate);
      browser.sleep(2000);
      this.selectDate.click();
  }
  // Product view
    this.productView = function (){
      helper.waitUntilReady(this.newbookNow);
      spaElements.newbookNow.click();

  }
}

module.exports = CruiseSearch;
