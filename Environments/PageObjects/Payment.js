var helper = require('../../helper.js');


var PaymentPage = function() {
  //objects
  this.paymentHomepage = element(by.css('a[class*="aem-Grid aem-Grid--12 aem-Grid--default--12"]'));
  this.alreadyBooked = element(by.css('a[href*="#rcl-submenu-3"]'));

  //this.paymentHomepage =element(by.css('body > div.page__main > div:nth-child(6) > footer > div.f_wrapper > div.links > div > div:nth-child(4) > ul > li:nth-child(4) > a'));
  this.resNumber = element(by.css('input[name*="bookingId"]'));
  this.lastName = element(by.css('input[name*="lastName"]'));
  this.button = element(by.css('button[class*="payment-form-base__secondarySubmit"]'));

  // Retrieve Booking
  this.navigateBooking = function() {
    helper.waitElementToBeClickable(this.alreadyBooked);
    this.alreadyBooked.click();
    browser.sleep(2000);
    this.paymentHomepage.click();
  }
  this.retrieveBooking = function() {
    helper.waitUntilReady(this.resNumber);
    this.resNumber.sendKeys('4316349');
    this.lastName.sendKeys('Test');
    element(by.css('select[id="shipCode"]')).element(by.cssContainingText('option', 'Mariner Of The Seas')).click();
    element(by.css('select[id="sailingDateMonth"]')).element(by.cssContainingText('option', 'January')).click();
    element(by.css('select[id="sailingDateDay"]')).element(by.cssContainingText('option', '4')).click();
    element(by.css('select[id="sailingDateYear"]')).element(by.cssContainingText('option', '2019')).click();
    browser.sleep(2000);
    this.button.click();
    browser.sleep(4000);
  }
  // Product view
  this.productView = function() {
    helper.waitUntilReady(this.newbookNow);
    spaElements.newbookNow.click();

  }
}

module.exports = PaymentPage;
