var helper = require('../../helper.js');
var a = Math.floor(Math.random() * 10);


var Stateroom = function(){
  //objects
  this.continueStateroom = element(by.css('[data-selector="stateroom-continue"]'));
  this.plusStateroom = element(by.css('[data-selector="stateroom-plus"]'));
  this.minusStateroom = element(by.css('[data-selector="stateroom-minus"]'));

  // One Room
    this.stateroom_one = function (){
      helper.waitUntilReady(this.continueStateroom);
      this.continueStateroom.click();
  }
  // Two Rooms
    this.stateroom_two = function (){
      helper.waitUntilReady(this.plusStateroom);
      this.plusStateroom.click();
      helper.waitUntilReady(this.continueStateroom);
      this.continueStateroom.click();

  }
  // 3 Rooms
    this.stateroom_three = function (){
      helper.waitUntilReady(this.plusStateroom);
      this.plusStateroom.click();
      browser.sleep(2000);
      this.plusStateroom.click();
      helper.waitUntilReady(this.continueStateroom);
      this.continueStateroom.click();

  }
  // 4 Rooms
    this.stateroom_four = function (){
      helper.waitUntilReady(this.plusStateroom);
      this.plusStateroom.click();
      browser.sleep(2000);
      this.plusStateroom.click();
      browser.sleep(2000);
      this.plusStateroom.click();
      helper.waitUntilReady(this.continueStateroom);
      this.continueStateroom.click();

  }
}

module.exports = Stateroom;
