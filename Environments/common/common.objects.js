var helper = require('../../helper.js');
var faker = require('faker');

var CommonElements = function() {
  //AEM
  this.searchButton = element(by.id('find-a-cruise'));

  //Faker data
  this.randomFirstName = faker.name.firstName();
  this.randomLastName = faker.name.lastName();
  this.randomEmail = faker.internet.email();
  this.randomPhoneNumber = faker.phone.phoneNumber();

  //Data-selector
  this.showLogin = element(by.css('#main > page-guest-info > view-guest-info > div > div.column.small-12.large-8 > form-sign-in > div > div > h2'));
  this.usernameField = element(by.id('username'));
  this.passwordField = element(by.id('password'));
  this.sumbitGuestInfo = element(by.css('[data-selector="guestInfo-sign-in-continue"]'));

  this.nextGuest = element(by.id('button_1'));
  this.multinextGuest = element(by.css('#guest-panel-0 > div'));
  this.multinextGuest2 = element(by.css('#guest-panel-0 > div > div'));

  this.firstnameField = element(by.css('[data-selector="guestInfo-primary-first-name"]'));
  this.gdpFirstnameField = element(by.id('682d5789-876a-4cbc-acde-9042b4809752'));
  this.multiFirstnameField = element(by.css('input[name="firstName_1"]'));
  this.firstnameField2 = element(by.css('[data-selector="guestInfo-guest-2-first-name"]'));
  this.multifirstnameField2 = element(by.css('input[name="firstName_2"]'));


  this.lastnameField = element(by.css('[data-selector="guestInfo-primary-last-name"]'));
  this.gdpLastnameField = element(by.id('2637b057-217e-40d2-b34e-97321b736cc8'));
  this.multiLastnameField = element(by.css('input[name="lastName_1"]'));
  this.lastnameField2 = element(by.css('[data-selector="guestInfo-guest-2-last-name"]'));
  this.multilastnameField2 = element(by.css('input[name="lastName_2"]'));


  this.phonenumberField = element(by.css('[data-selector="guestInfo-primary-phone"]'));
  this.preprodPhonenumber = element(by.css('[data-selector="guestInfo-primary-phone"]'));
  this.gdpPhonenumberField = element(by.id('9c69e982-8683-486a-b345-f1717ee835db'));
  this.multiPhonenumberField = element(by.css('input[name="phoneNumber_1"]'));

  this.emailField = element(by.css('[data-selector="guestInfo-primary-email"]'));
  this.gdpEmailField = element(by.id('15653141-2457-4d25-bafb-13b98a3c74f4'));
  this.multiEmailField = element(by.css('input[name="email_1"]'));
  this.optionLabel = element(by.css('span[class*="optional"]'));

  //CSS
  this.continueGuest = element(by.css('[data-selector="guestInfo-guest-2-continue"]'));
  this.continueGuestMulti = element(by.css('#guest-panel-1 > div > button'));
  this.continueGuestBRA = element(by.css('div[class="next-guest-btn button right"]'));
  this.continueGuestBRA2 = element(by.css('button[class="primary right selectAndContinue"]'));
  this.continueGuest2 = element(by.css('#guest-panel-1 > div > button'));
  //this.continueGuest2Multi =element(by.css('a[class*="next-guest-btn button right"]'));
  this.noPPGR = element(by.css('#preferences > section.preferences-container.column.small-12.large-8 > form > section.row.vacation-protection > section.vacation-protection-options.columns.small-12.medium-6.text-right > div > div:nth-child(2) > app-radio > div > label'));
  this.noPPGR_Multi = element(by.css('input.price-reload[name="vacationProtection"][value="FALSE"]'));
  this.continuePref = element(by.css('[data-selector="preferences-continue"]'));
  this.continuePref_Multi = element(by.css('#holdOrContinue > div.medium-6.large-3.small-12.columns.right > button'));
  this.continuePref_BRA = element(by.css('button[type="submit"]'));
  this.viewPricingbyGuest = element(by.css('[data-selector="summary-view-pricing-by-guest"]'));

  this.changeItinerary = element(by.css('body > app-root > header > div.row.small-collapse.align-middle > div.columns.cruise-info > div > div:nth-child(2) > div.change-itinerary.show-for-large > a'));
  this.changeItineraryMulti = element(by.css('svg[class*="crown-anchor"]'));
  //this.multichangeItinerary =element(by.css('body > header > div.booking-header-itinerary.left > div > a'));

  this.understandButton = element(by.css('#module-modal > div > div > div:nth-child(2) > button > span'));
  this.understandButton_Multi = element(by.css('a[class*="ok-action button primary"]'));

  //this.multiUnderstandButton =element(by.css('button[class*=\'button submit center\']'));

  this.gdpNextGuest = element(by.css('#guest-panel-0 > div > div'));

  this.creditcardNumber = element(by.id('creditCardNumber_1'));
  this.creditcardFirstName = element(by.id('firstName_1'));
  this.creditcardLastName = element(by.id('lastName_1'));
  this.acceptTAC = element(by.css('#main > page-payment > view-payment > div > div.column.small-12.large-8 > app-form-payment > div > form > div:nth-child(3) > div > label'));
  this.submitPayment = element(by.css('#main > page-payment > view-payment > div > div.column.small-12.large-8 > app-form-payment > div > form > div.row.column.clearfix > button'));
  this.logoButton = element(by.xpath('//*[@id="logo"]'));
}



module.exports = CommonElements;
