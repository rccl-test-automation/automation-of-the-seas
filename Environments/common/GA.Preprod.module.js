//redirects.module.js
'use strict';
module.exports = [
  {
      "Country": "US",
      "Original": "https://www.new.royalcaribbean.com/?wuc=USA",
      "CruiseSearch":"https://test:royal@secure.new.royalcaribbean.com/cruises",
      "Create": "https://secure.new.royalcaribbean.com/mycruises/createMyCruisesProfile.do",
      "MyCruises": "https://secure.new.royalcaribbean.com/mycruises/home.do?cS=MHDR",
      "CruisePlanner": "https://secure.new.royalcaribbean.com/mycruises/login.do?default=false&cS=MHDR",
      "Itineraries": "https://secure.new.royalcaribbean.com/mycruises/savedItineraries.do",
      "Purchased_Held_Cruises": "https://secure.new.royalcaribbean.com/mycruises/activeReservations.do",
      "SavedCruises": "https://secure.new.royalcaribbean.com/mycruises/savedItineraries.do",
      "CompletedCruises": "https://secure.new.royalcaribbean.com/mycruises/completedCruises.do",
      "Profile": "https://secure.new.royalcaribbean.com/mycruises/personalInfo.do",
      "PersonalInformation": "https://secure.new.royalcaribbean.com/mycruises/personalInfo.do",
      "GuestInformation": "https://secure.new.royalcaribbean.com/mycruises/selectGuest.do",
      "AddGuest": "https://secure.new.royalcaribbean.com/mycruises/addGuest.do",
      "Enroll_Activate": "https://secure.new.royalcaribbean.com/mycruises/cas/prepareEnrollOthers.do",
      "Crown_Anchor_Enrollment": "https://secure.new.royalcaribbean.com/mycruises/casEnrollment.do",
      "Change_Username_Password": "https://secure.new.royalcaribbean.com/mycruises/changePassword.do",
      "CruisingPreferences": "https://secure.new.royalcaribbean.com/mycruises/cruisingPreferences.do",
      "OnboardPreferences": "https://secure.new.royalcaribbean.com/mycruises/onboardPrefences.do",
      "UpgradePreferences": "https://secure.new.royalcaribbean.com/mycruises/upgradePreferences.do",
      "GiftPreferences": "https://secure.new.royalcaribbean.com/mycruises/getPreferences.do",
      "MakeaPayment": "https://secure.new.royalcaribbean.com/booking/retrieveBookingMyCruises.do?tabName=payment&bookingId=2375289",
      "Reservation": "https://secure.new.royalcaribbean.com/mycruises/bookingLookup.do",
      "Messages": "https://secure.new.royalcaribbean.com/mycruises/gmc/getMessages.do",
      "Friend": "https://secure.new.royalcaribbean.com/mycruises/displayCreateReferral.do",
      "Redirects": "https://www.new.royalcaribbean.com/account/signin",
      "CreateRedirect": "https://www.new.royalcaribbean.com/account/create"
    },
    {
      "Country": "Spain",
      "Original": "https://www.new.royalcaribbean.es/?wuc=ESP",
      "CruiseSearch":"https://test:royal@secure.new.royalcaribbean.es/cruises",
      "Create": "https://secure.new.royalcaribbean.es/mycruises/createMyCruisesProfile.do",
      "MyCruises": "https://secure.new.royalcaribbean.es/mycruises/home.do?cS=MHDR",
      "CruisePlanner": "https://secure.new.royalcaribbean.es/mycruises/login.do?default=false&cS=MHDR",
      "Itineraries": "https://secure.new.royalcaribbean.es/mycruises/savedItineraries.do",
      "Purchased_Held_Cruises": "https://secure.new.royalcaribbean.es/mycruises/activeReservations.do",
      "SavedCruises": "https://secure.new.royalcaribbean.es/mycruises/savedItineraries.do",
      "CompletedCruises": "https://secure.new.royalcaribbean.es/mycruises/completedCruises.do",
      "Profile": "https://secure.new.royalcaribbean.es/mycruises/personalInfo.do",
      "PersonalInformation": "https://secure.new.royalcaribbean.es/mycruises/personalInfo.do",
      "GuestInformation": "https://secure.new.royalcaribbean.es/mycruises/selectGuest.do",
      "AddGuest": "https://secure.new.royalcaribbean.es/mycruises/addGuest.do",
      "Enroll_Activate": "https://secure.new.royalcaribbean.es/mycruises/cas/prepareEnrollOthers.do",
      "Crown_Anchor_Enrollment": "https://secure.new.royalcaribbean.es/mycruises/casEnrollment.do",
      "Change_Username_Password": "https://secure.new.royalcaribbean.es/mycruises/changePassword.do",
      "CruisingPreferences": "https://secure.new.royalcaribbean.es/mycruises/cruisingPreferences.do",
      "OnboardPreferences": "https://secure.new.royalcaribbean.es/mycruises/onboardPrefences.do",
      "UpgradePreferences": "https://secure.new.royalcaribbean.es/mycruises/upgradePreferences.do",
      "GiftPreferences": "https://secure.new.royalcaribbean.es/mycruises/getPreferences.do",
      "MakeaPayment": "https://secure.new.royalcaribbean.es/booking/retrieveBookingMyCruises.do?tabName=payment&bookingId=2375289",
      "Reservation": "https://secure.new.royalcaribbean.es/mycruises/bookingLookup.do",
      "Messages": "https://secure.new.royalcaribbean.es/mycruises/gmc/getMessages.do",
      "Friend": "https://secure.new.royalcaribbean.es/mycruises/displayCreateReferral.do",
      "Redirects": "https://www.new.royalcaribbean.es/account/signin",
      "CreateRedirect": "https://www.new.royalcaribbean.com/account/create"
    },
    {
      "Country": "LACAR",
      "Original": "https://www.new.royalcaribbean-espanol.com/?wuc=ARG",
      "CruiseSearch":"https://test:royal@secure.new.royalcaribbean-espanol.com/cruises",
      "Create": "https://secure.new.royalcaribbean-espanol.com/mycruises/createMyCruisesProfile.do",
      "MyCruises": "https://secure.new.royalcaribbean-espanol.com/mycruises/home.do?cS=MHDR",
      "CruisePlanner": "https://secure.new.royalcaribbean-espanol.com/mycruises/login.do?default=false&cS=MHDR",
      "Itineraries": "https://secure.new.royalcaribbean-espanol.com/mycruises/savedItineraries.do",
      "Purchased_Held_Cruises": "https://secure.new.royalcaribbean-espanol.com/mycruises/activeReservations.do",
      "SavedCruises": "https://secure.new.royalcaribbean-espanol.com/mycruises/savedItineraries.do",
      "CompletedCruises": "https://secure.new.royalcaribbean-espanol.com/mycruises/completedCruises.do",
      "Profile": "https://secure.new.royalcaribbean-espanol.com/mycruises/personalInfo.do",
      "PersonalInformation": "https://secure.new.royalcaribbean-espanol.com/mycruises/personalInfo.do",
      "GuestInformation": "https://secure.new.royalcaribbean-espanol.com/mycruises/selectGuest.do",
      "AddGuest": "https://secure.new.royalcaribbean-espanol.com/mycruises/addGuest.do",
      "Enroll_Activate": "https://secure.new.royalcaribbean-espanol.com/mycruises/cas/prepareEnrollOthers.do",
      "Crown_Anchor_Enrollment": "https://secure.new.royalcaribbean-espanol.com/mycruises/casEnrollment.do",
      "Change_Username_Password": "https://secure.new.royalcaribbean-espanol.com/mycruises/changePassword.do",
      "CruisingPreferences": "https://secure.new.royalcaribbean-espanol.com/mycruises/cruisingPreferences.do",
      "OnboardPreferences": "https://secure.new.royalcaribbean-espanol.com/mycruises/onboardPrefences.do",
      "UpgradePreferences": "https://secure.new.royalcaribbean-espanol.com/mycruises/upgradePreferences.do",
      "GiftPreferences": "https://secure.new.royalcaribbean-espanol.com/mycruises/getPreferences.do",
      "MakeaPayment": "https://secure.new.royalcaribbean-espanol.com/booking/retrieveBookingMyCruises.do?tabName=payment&bookingId=2375289",
      "Reservation": "https://secure.new.royalcaribbean-espanol.com/mycruises/bookingLookup.do",
      "Messages": "https://secure.new.royalcaribbean-espanol.com/mycruises/gmc/getMessages.do",
      "Friend": "https://secure.new.royalcaribbean-espanol.com/mycruises/displayCreateReferral.do",
      "Redirects": "https://www.new.royalcaribbean-espanol.com/account/signin",
      "CreateRedirect": "https://www.new.royalcaribbean.com/account/create"
    },
    // {
    //   "Country": "Australia",
    //   "Original": "https://www.new.royalcaribbean.com.au/?wuc=AUS",
    //   "CruiseSearch": "https://test:royal@secure.new.royalcaribbean.com.au/cruises",
    //   "Create": "https://secure.new.royalcaribbean.com.au/mycruises/createMyCruisesProfile.do",
    //   "MyCruises": "https://secure.new.royalcaribbean.com.au/mycruises/home.do?cS=MHDR",
    //   "CruisePlanner": "https://secure.new.royalcaribbean.com.au/mycruises/login.do?default=false&cS=MHDR",
    //   "Itineraries": "https://secure.new.royalcaribbean.com.au/mycruises/savedItineraries.do",
    //   "Purchased_Held_Cruises": "https://secure.new.royalcaribbean.com.au/mycruises/activeReservations.do",
    //   "SavedCruises": "https://secure.new.royalcaribbean.com.au/mycruises/savedItineraries.do",
    //   "CompletedCruises": "https://secure.new.royalcaribbean.com.au/mycruises/completedCruises.do",
    //   "Profile": "https://secure.new.royalcaribbean.com.au/mycruises/personalInfo.do",
    //   "PersonalInformation": "https://secure.new.royalcaribbean.com.au/mycruises/personalInfo.do",
    //   "GuestInformation": "https://secure.new.royalcaribbean.com.au/mycruises/selectGuest.do",
    //   "AddGuest": "https://secure.new.royalcaribbean.com.au/mycruises/addGuest.do",
    //   "Enroll_Activate": "https://secure.new.royalcaribbean.com.au/mycruises/cas/prepareEnrollOthers.do",
    //   "Crown_Anchor_Enrollment": "https://secure.new.royalcaribbean.com.au/mycruises/casEnrollment.do",
    //   "Change_Username_Password": "https://secure.new.royalcaribbean.com.au/mycruises/changePassword.do",
    //   "CruisingPreferences": "https://secure.new.royalcaribbean.com.au/mycruises/cruisingPreferences.do",
    //   "OnboardPreferences": "https://secure.new.royalcaribbean.com.au/mycruises/onboardPrefences.do",
    //   "UpgradePreferences": "https://secure.new.royalcaribbean.com.au/mycruises/upgradePreferences.do",
    //   "GiftPreferences": "https://secure.new.royalcaribbean.com.au/mycruises/getPreferences.do",
    //   "MakeaPayment": "https://secure.new.royalcaribbean.com.au/booking/retrieveBookingMyCruises.do?tabName=payment&bookingId=2375289",
    //   "Reservation": "https://secure.new.royalcaribbean.com.au/mycruises/bookingLookup.do",
    //   "Messages": "https://secure.new.royalcaribbean.com.au/mycruises/gmc/getMessages.do",
    //   "Friend": "https://secure.new.royalcaribbean.com.au/mycruises/displayCreateReferral.do",
    //   "Redirects": "https://www.new.royalcaribbean.com.au/account/signin",
    //   "CreateRedirect": "https://www.new.royalcaribbean.com/account/create"
    // },
    {
      "Country": "Brasil",
      "Original": "https://www.new.royalcaribbean.com.br/?wuc=BRA",
      "CruiseSearch":"https://test:royal@secure.new.royalcaribbean.com.br/cruises",
      "Create": "https://secure.new.royalcaribbean.com.br/mycruises/createMyCruisesProfile.do",
      "MyCruises": "https://secure.new.royalcaribbean.com.br/mycruises/home.do?cS=MHDR",
      "CruisePlanner": "https://secure.new.royalcaribbean.com.br/mycruises/login.do?default=false&cS=MHDR",
      "Itineraries": "https://secure.new.royalcaribbean.com.br/mycruises/savedItineraries.do",
      "Purchased_Held_Cruises": "https://secure.new.royalcaribbean.com.br/mycruises/activeReservations.do",
      "SavedCruises": "https://secure.new.royalcaribbean.com.br/mycruises/savedItineraries.do",
      "CompletedCruises": "https://secure.new.royalcaribbean.com.br/mycruises/completedCruises.do",
      "Profile": "https://secure.new.royalcaribbean.com.br/mycruises/personalInfo.do",
      "PersonalInformation": "https://secure.new.royalcaribbean.com.br/mycruises/personalInfo.do",
      "GuestInformation": "https://secure.new.royalcaribbean.com.br/mycruises/selectGuest.do",
      "AddGuest": "https://secure.new.royalcaribbean.com.br/mycruises/addGuest.do",
      "Enroll_Activate": "https://secure.new.royalcaribbean.com.br/mycruises/cas/prepareEnrollOthers.do",
      "Crown_Anchor_Enrollment": "https://secure.new.royalcaribbean.com.br/mycruises/casEnrollment.do",
      "Change_Username_Password": "https://secure.new.royalcaribbean.com.br/mycruises/changePassword.do",
      "CruisingPreferences": "https://secure.new.royalcaribbean.com.br/mycruises/cruisingPreferences.do",
      "OnboardPreferences": "https://secure.new.royalcaribbean.com.br/mycruises/onboardPrefences.do",
      "UpgradePreferences": "https://secure.new.royalcaribbean.com.br/mycruises/upgradePreferences.do",
      "GiftPreferences": "https://secure.new.royalcaribbean.com.br/mycruises/getPreferences.do",
      "MakeaPayment": "https://secure.new.royalcaribbean.com.br/booking/retrieveBookingMyCruises.do?tabName=payment&bookingId=2375289",
      "Reservation": "https://secure.new.royalcaribbean.com.br/mycruises/bookingLookup.do",
      "Messages": "https://secure.new.royalcaribbean.com.br/mycruises/gmc/getMessages.do",
      "Friend": "https://secure.new.royalcaribbean.com.br/mycruises/displayCreateReferral.do",
      "Redirects": "https://www.new.royalcaribbean.com.br/account/signin",
      "CreateRedirect": "https://www.new.royalcaribbean.com/account/create"
    }
  ]
