var helper = require('../../helper.js');
var faker =require('faker');
var LegacyElements = require('../common/legacy.objects.js');
var legacyElements = new LegacyElements();

var LegacyFunctions = function(){

  //functions

  //MyCruises Log in
    this.hpLogin = function (){
      helper.waitUntilReady(legacyElements.login);
      legacyElements.login.click();
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toEqual('https://www.royalcaribbean.com/account/signin');
      helper.waitUntilReady(legacyElements.usernameFieldGA);
      legacyElements.usernameFieldGA.sendKeys('jasontur@rccl.com');
      helper.waitUntilReady(legacyElements.passwordField);
      legacyElements.passwordField.sendKeys('password1');
      helper.waitUntilReady(legacyElements.loginButtonGA);
      legacyElements.loginButtonGA.click();
      helper.waitUntilReady(legacyElements.gaName);
      expect(legacyElements.gaName.getText()).toBe('Jason');
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toEqual('https://www.royalcaribbean.com/account/');
  }
  //MyCruises Log in--PREPROD
    this.hpLoginPreprod = function (){
      helper.waitUntilReady(legacyElements.hpLogin);
      legacyElements.hpLogin.click();
      helper.waitUntilReady(legacyElements.hpLoginButton);
      legacyElements.hpLoginButton.click();
      expect(browser.getCurrentUrl()).toEqual('https://secure.new.royalcaribbean.com/mycruises/login.do');
      helper.waitUntilReady(legacyElements.usernameField);
      legacyElements.usernameField.sendKeys('jasontur@rccl.com');
      helper.waitUntilReady(legacyElements.passwordField);
      legacyElements.passwordField.sendKeys('Test1234');
      helper.waitUntilReady(legacyElements.loginButton);
      legacyElements.loginButton.click();
      expect(browser.getCurrentUrl()).toEqual('https://secure.new.royalcaribbean.com/mycruises/processLogin.do');
  }
  //MyCruises Log in--Stage2
    this.hpLoginStage2 = function (){
      helper.waitUntilReady(legacyElements.hpLogin);
      legacyElements.hpLogin.click();
      browser.sleep(4000);
      expect(browser.getCurrentUrl()).toContain('https://www.stage2.royalcaribbean.com/account/signin/');
      helper.waitUntilReady(legacyElements.usernameFieldGA);
      legacyElements.usernameFieldGA.sendKeys('edleeash3@comcast.net');
      helper.waitUntilReady(legacyElements.passwordField);
      legacyElements.passwordField.sendKeys('Royal123');
      helper.waitUntilReady(legacyElements.loginButtonGA);
      legacyElements.loginButtonGA.click();
      browser.sleep(4000);
      expect(browser.getCurrentUrl()).toContain('https://www.stage2.royalcaribbean.com/account');
  }
  //OLCI Log in
    this.olciLogin = function (){
      helper.waitUntilReady(legacyElements.alreadyBooked);
      legacyElements.alreadyBooked.click();
      helper.waitUntilReady(legacyElements.olciHomePage);
      legacyElements.olciHomePage.click();
      expect(browser.getCurrentUrl()).toEqual('https://secure.royalcaribbean.com/beforeyouboard/boardingDocuments.do?icid=hpnvbr_pagehe_nln_hm_awaren_954');
      helper.waitUntilReady(legacyElements.lastnameField);
      legacyElements.lastnameField.sendKeys('Test');
      helper.waitUntilReady(legacyElements.resNumber);
      legacyElements.resNumber.sendKeys('4316349');
      element(by.css('select[name="departureDay"]')).element(by.cssContainingText('option', '4')).click();
      element(by.css('select[name="departureMonth"]')).element(by.cssContainingText('option', 'January')).click();
      element(by.css('select[name="departureYear"]')).element(by.cssContainingText('option', '2019')).click();
      element(by.css('select[name="shipCode"]')).element(by.cssContainingText('option', 'Mariner Of The Seas')).click();
      helper.waitUntilReady(legacyElements.olciloginButton);
      legacyElements.olciloginButton.click();
  }
  //OLCI Log in--PREPROD
    this.olciLoginPreprod = function (){
      helper.waitUntilReady(legacyElements.alreadyBooked);
      legacyElements.alreadyBooked.click();
      helper.waitUntilReady(legacyElements.olciHomePage);
      legacyElements.olciHomePage.click();
      expect(browser.getCurrentUrl()).toEqual('https://secure.new.royalcaribbean.com/beforeyouboard/boardingDocuments.do?icid=hpnvbr_pagehe_nln_hm_awaren_954');
      // helper.waitUntilReady(legacyElements.usernameField);
      // legacyElements.usernameField.sendKeys('jasontur@rccl.com');
      // helper.waitUntilReady(legacyElements.passwordField);
      // legacyElements.passwordField.sendKeys('Test1234');
      // helper.waitUntilReady(legacyElements.casloginButton);
      // legacyElements.casloginButton.click();
  }
  //Cruise planner
    this.cpLogin = function (){
      helper.waitUntilReady(legacyElements.lastnameField);
      legacyElements.lastnameField.sendKeys('Test');
      legacyElements.resNumber.sendKeys('4316349');
      element(by.css('select[name="departureDay"]')).element(by.cssContainingText('option', '4')).click();
      element(by.css('select[name="departureMonth"]')).element(by.cssContainingText('option', 'January')).click();
      element(by.css('select[name="departureYear"]')).element(by.cssContainingText('option', '2019')).click();
      element(by.css('select[name="shipCode"]')).element(by.cssContainingText('option', 'Mariner Of The Seas')).click();
      legacyElements.cpSubmitbutton.click();
      browser.sleep(4000);
      expect(browser.getCurrentUrl()).toContain('https://secure.royalcaribbean.com/cruiseplanner/');
  }
}

module.exports = LegacyFunctions;
