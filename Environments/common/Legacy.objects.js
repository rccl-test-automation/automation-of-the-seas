var helper = require('../../helper.js');
var faker =require('faker');

var LegacyElements = function(){

  //Homepage
  this.hpLogin =element(by.css('#aemHeader > header > nav > div > div > nav > div.login_area.aem-GridColumn--phone--12.aem-GridColumn--tablet--12.aem-GridColumn.aem-GridColumn--default--3 > div.show-for-desktop.show-for-phone.login.aem-Grid.aem-Grid--12.aem-Grid--phone--12.aem-Grid--tablet--12.aem-Grid--default--2 > div > div > div.logged-out.active > a'));
  this.login =element(by.css('div[class*="show-for-desktop show-for-phone login aem-Grid aem-Grid--12 aem-Grid--phone--12 aem-Grid--tablet--12 aem-Grid--default--2"]'));
  this.login2 =element(by.css('#rcl-submenu-logout > div > div:nth-child(2)'));
  this.gaLogin =element(by.css('body > div.page__main > div:nth-child(3) > header > nav > div > div > nav > div.login_area.aem-GridColumn--phone--12.aem-GridColumn--tablet--12.aem-GridColumn.aem-GridColumn--default--3 > div.show-for-desktop.show-for-phone.login.aem-Grid.aem-Grid--12.aem-Grid--phone--12.aem-Grid--tablet--12.aem-Grid--default--2 > div > div > div.logged-out.active > a'));
  this.hpLoginButtonCAS =element(by.css('a[href*="/cas/login.do"]'));
  this.alreadyBooked =element(by.css('a[href*="#rcl-submenu-3"]'));
  this.gaName =element(by.css('h2[class*="dashboard-header__user-name"]'));
  this.Brochure =element(by.css('a[href*="/customersupport/brochuresandmedia/order.do"]'));
  this.olciHomePage =element(by.css('a[href*="/beforeyouboard/boardingDocuments.do?icid=hpnvbr_pagehe_nln_hm_awaren_954"]'));

  //Mycruises/CAS/OLCI
  this.usernameField =element(by.css('input[name="userId"]'));
  this.lastnameField =element(by.css('input[name="lastName"]'));
  this.passwordField =element(by.css('input[name="password"]'));
  this.usernameFieldGA =element(by.css('input[name="email"]'));
  this.passwordFieldGA =element(by.css('input[name="password"]'));
  this.resNumber =element(by.css('input[name="bookingId"]'));
  this.loginButton =element(by.css('#pageContent > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > form > table > tbody > tr:nth-child(10) > td.Pad21cSm > input[type="image"]'));
  this.loginButtonGA =element(by.xpath('//*[@id="start-of-content"]/app-login/div[2]/div/div/div[3]/div[2]/button'));

  this.casloginButton =element(by.css('#rccas-login > div > div.login-main > div > ol > li > input'));
  this.olciloginButton =element(by.id('login_submit'));
  this.cpSubmitbutton =element(by.css('input[alt="Submit"]'));
}

module.exports = LegacyElements;
