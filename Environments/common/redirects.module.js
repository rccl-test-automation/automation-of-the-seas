//redirects.module.js
'use strict';
module.exports = [
  {
    "Country": "USA",
    "Original": "https://www.stage2.royalcaribbean.com/?wuc=USA",
    "CruiseSearch":"https://test:royal@secure.stage2.royalcaribbean.com/cruises",
    "GuestAccount": "https://www.stage2.royalcaribbean.com/account/signin",
    "Account" : "https://www.stage2.royalcaribbean.com/account/",
  },
  {
    "Country": "Spain",
    "Original": "https://www.stage2.royalcaribbean.es/?wuc=ESP",
    "CruiseSearch":"https://test:royal@secure.stage2.royalcaribbean.es/cruises",
    "GuestAccount": "https://www.stage2.royalcaribbean.es/account/signin",
    "Account" : "https://www.stage2.royalcaribbean.es/account/",
  },
  {
    "Country": "Lacar(Argentina)",
    "Original": "https://www.stage2.royalcaribbean-espanol.com/?wuc=ARG",
    "CruiseSearch":"https://test:royal@secure.stage2.royalcaribbean-espanol.com/cruises",
    "GuestAccount": "https://www.stage2.royalcaribbean-espanol.com/account/signin",
    "Account" : "https://www.stage2.royalcaribbean-espanol.com/account/",
  },
  {
    "Country": "Australia",
    "Original": "https://www.stage2.royalcaribbean.com.au/?wuc=AUS",
    "CruiseSearch": "https://test:royal@secure.stage2.royalcaribbean.com.au/cruises",
    "Secure":"https://test:royal@secure.stage2.royalcaribbean.com.au/cruises",
    "GuestAccount": "https://www.stage2.royalcaribbean.com.au/account/signin",
    "Account" : "https://www.stage2.royalcaribbean.au/account/"
  },
  {
    "Country": "Brasil",
    "Original": "https://www.stage2.royalcaribbean.com.br/?wuc=BRA",
    "CruiseSearch":"https://test:royal@secure.stage2.royalcaribbean.com.br/cruises",
    "Secure":"https://secure.stage2.royalcaribbean.com.br/mycruises/displayCreateReferral.do",
    "GuestAccount": "https://www.stage2.royalcaribbean.com.br/account/signin",
    "Account" : "https://www.stage2.royalcaribbean.br/account/",
  },
  {
    "Country": "Mexico",
    "Original": "https://www.stage2.royalcaribbean-espanol.com/mex.html?wuc=MEX",
    "CruiseSearch":"https://test:royal@secure.stage2.royalcaribbean.com-espanol.com/cruises",
    "GuestAccount": "https://www.stage2.royalcaribbean-espanol.com/account/signin",
    "Account" : "https://www.stage2.royalcaribbean-espanol.com/account/",
  }
]
