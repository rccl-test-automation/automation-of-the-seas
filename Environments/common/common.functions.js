var helper = require('../../helper.js');
var faker = require('faker');
var CommonElements = require('../common/common.objects.js');
var commonElements = new CommonElements();

var CommonFunctions = function() {
  //functions

  //Guest Info
  this.guestInfo = function() {
    console.log();
    helper.waitUntilReady(commonElements.firstnameField);
    commonElements.firstnameField.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.lastnameField);
    commonElements.lastnameField.sendKeys(commonElements.randomLastName);

    helper.waitUntilReady(commonElements.phonenumberField);
    commonElements.phonenumberField.sendKeys('7865551234');

    helper.waitUntilReady(commonElements.emailField);
    commonElements.emailField.sendKeys(commonElements.randomEmail);

  //  browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[id="gender_1"]')).element(by.cssContainingText('option', 'Male')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    element(by.css('select[id="residents_1"]')).element(by.cssContainingText('option', 'FLORIDA')).click();
    element(by.css('select[id="month_1"]')).element(by.cssContainingText('option', 'March')).click();
    element(by.css('select[id="day_1"]')).element(by.cssContainingText('option', '06')).click();
    element(by.css('select[id="year_1"]')).element(by.cssContainingText('option', '1984')).click();

    helper.waitUntilReady(commonElements.nextGuest);
    commonElements.nextGuest.click();
    helper.waitUntilReady(commonElements.firstnameField2);
    commonElements.firstnameField2.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.lastnameField2);
    commonElements.lastnameField2.sendKeys(commonElements.randomLastName);

    //Dropdowns
    browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[id="gender_2"]')).element(by.cssContainingText('option', 'Female')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    element(by.css('select[id="residents_2"]')).element(by.cssContainingText('option', 'FLORIDA')).click();
    element(by.css('select[id="month_2"]')).element(by.cssContainingText('option', 'June')).click();
    element(by.css('select[id="day_2"]')).element(by.cssContainingText('option', '10')).click();
    element(by.css('select[id="year_2"]')).element(by.cssContainingText('option', '1983')).click();
    console.log();
    helper.waitUntilReady(commonElements.continueGuest);
    commonElements.continueGuest.click();
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.noPPGR);
    commonElements.noPPGR.click();
    browser.sleep(4000);
    helper.waitUntilReady(commonElements.continuePref);
    commonElements.continuePref.click();
  }

  //Guest Info Spanish
  this.guestInfoSpanish = function() {
    helper.waitUntilReady(commonElements.firstnameField);
    commonElements.firstnameField.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.lastnameField);
    commonElements.lastnameField.sendKeys(commonElements.randomLastName);

    helper.waitUntilReady(commonElements.phonenumberField);
    commonElements.phonenumberField.sendKeys('7865551234');

    helper.waitUntilReady(commonElements.emailField);
    commonElements.emailField.sendKeys(commonElements.randomEmail);

  //  browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[id="gender_1"]')).element(by.cssContainingText('option', 'Hombre')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    //element(by.css('select[id="residents_1"]')).element(by.cssContainingText('option', 'FLORIDA')).click();
    element(by.css('select[id="day_1"]')).element(by.cssContainingText('option', '06')).click();
    element(by.css('select[id="month_1"]')).element(by.cssContainingText('option', 'Marzo')).click();
    element(by.css('select[id="year_1"]')).element(by.cssContainingText('option', '1984')).click();

    helper.waitUntilReady(commonElements.nextGuest);
    commonElements.nextGuest.click();
    helper.waitUntilReady(commonElements.firstnameField2);
    commonElements.firstnameField2.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.lastnameField2);
    commonElements.lastnameField2.sendKeys(commonElements.randomLastName);

    //Dropdowns
    browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[id="gender_2"]')).element(by.cssContainingText('option', 'Hombre')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    //element(by.css('select[id="residents_2"]')).element(by.cssContainingText('option', 'FLORIDA')).click();
    element(by.css('select[id="day_2"]')).element(by.cssContainingText('option', '10')).click();
    element(by.css('select[id="month_2"]')).element(by.cssContainingText('option', 'Marzo')).click();
    element(by.css('select[id="year_2"]')).element(by.cssContainingText('option', '1983')).click();

    helper.waitUntilReady(commonElements.continueGuest);
    commonElements.continueGuest.click();
    browser.sleep(2000);
    // helper.waitUntilReady(commonElements.noPPGR);
    // commonElements.noPPGR.click();
    // browser.sleep(4000);
    helper.waitUntilReady(commonElements.continuePref);
    commonElements.continuePref.click();
  }
  //Guest Info AUS
  this.guestInfoAUS = function() {
    helper.waitUntilReady(commonElements.firstnameField);
    commonElements.firstnameField.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.lastnameField);
    commonElements.lastnameField.sendKeys(commonElements.randomLastName);

    helper.waitUntilReady(commonElements.phonenumberField);
    commonElements.phonenumberField.sendKeys('7865551234');

    helper.waitUntilReady(commonElements.emailField);
    commonElements.emailField.sendKeys(commonElements.randomEmail);

  //  browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[id="gender_1"]')).element(by.cssContainingText('option', 'Male')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    //element(by.css('select[id="residents_1"]')).element(by.cssContainingText('option', 'FLORIDA')).click();
    element(by.css('select[id="day_1"]')).element(by.cssContainingText('option', '06')).click();
    element(by.css('select[id="month_1"]')).element(by.cssContainingText('option', 'March')).click();
    element(by.css('select[id="year_1"]')).element(by.cssContainingText('option', '1984')).click();

    helper.waitUntilReady(commonElements.nextGuest);
    commonElements.nextGuest.click();
    helper.waitUntilReady(commonElements.firstnameField2);
    commonElements.firstnameField2.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.lastnameField2);
    commonElements.lastnameField2.sendKeys(commonElements.randomLastName);

    //Dropdowns
    browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[id="gender_2"]')).element(by.cssContainingText('option', 'Female')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    //element(by.css('select[id="residents_2"]')).element(by.cssContainingText('option', 'FLORIDA')).click();
    element(by.css('select[id="day_2"]')).element(by.cssContainingText('option', '10')).click();
    element(by.css('select[id="month_2"]')).element(by.cssContainingText('option', 'June')).click();
    element(by.css('select[id="year_2"]')).element(by.cssContainingText('option', '1983')).click();

    helper.waitUntilReady(commonElements.continueGuest);
    commonElements.continueGuest.click();
    browser.sleep(2000);
    // helper.waitUntilReady(commonElements.noPPGR);
    // commonElements.noPPGR.click();
    // browser.sleep(4000);
    helper.waitUntilReady(commonElements.continuePref);
    commonElements.continuePref.click();
  }

  this.guestInfoBRA = function() {
    //Guest 1
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.multiFirstnameField);
    commonElements.multiFirstnameField.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.multiLastnameField);
    commonElements.multiLastnameField.sendKeys(commonElements.randomLastName);


    element(by.css('select[name="day_1"]')).element(by.cssContainingText('option', '06')).click();
    element(by.css('select[name="month_1"]')).element(by.cssContainingText('option', 'Março')).click();
    element(by.css('select[name="year_1"]')).element(by.cssContainingText('option', '1984')).click();

    element(by.css('select[name="gender_1"]')).element(by.cssContainingText('option', 'Masculino')).click();
    //element(by.css('select[name="residents_1"]')).element(by.cssContainingText('option', 'FLORIDA')).click();

    helper.waitUntilReady(commonElements.multiEmailField);
    commonElements.multiEmailField.sendKeys(commonElements.randomEmail);

    helper.waitUntilReady(commonElements.multiPhonenumberField);
    commonElements.multiPhonenumberField.sendKeys('7865551234');

    helper.waitUntilReady(commonElements.continueGuestBRA);
    commonElements.continueGuestBRA.click();

    //Guest 2
    helper.waitUntilReady(commonElements.multifirstnameField2);
    commonElements.multifirstnameField2.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.multilastnameField2);
    commonElements.multilastnameField2.sendKeys(commonElements.randomLastName);

    element(by.css('select[name="day_2"]')).element(by.cssContainingText('option', '10')).click();
    element(by.css('select[name="month_2"]')).element(by.cssContainingText('option', 'Março')).click();
    element(by.css('select[name="year_2"]')).element(by.cssContainingText('option', '1983')).click();

    //browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[name="gender_2"]')).element(by.cssContainingText('option', 'Masculino')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    //element(by.css('select[name="residents_2"]')).element(by.cssContainingText('option', 'FLORIDA')).click();

    helper.waitUntilReady(commonElements.continueGuestMulti);
    commonElements.continueGuestBRA2.click();
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.continueGuestBRA2);
    commonElements.continueGuestBRA2.click();
  }
  //Guest Info-- Multi
  this.guestInfoMulti = function() {
    //Room 1
    //Guest 1
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.multiFirstnameField);
    commonElements.multiFirstnameField.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.multiLastnameField);
    commonElements.multiLastnameField.sendKeys(commonElements.randomLastName);

    element(by.css('select[name="month_1"]')).element(by.cssContainingText('option', 'March')).click();
    element(by.css('select[name="day_1"]')).element(by.cssContainingText('option', '06')).click();
    element(by.css('select[name="year_1"]')).element(by.cssContainingText('option', '1984')).click();
    element(by.css('select[name="gender_1"]')).element(by.cssContainingText('option', 'Male')).click();
    element(by.css('select[name="residents_1"]')).element(by.cssContainingText('option', 'FLORIDA')).click();

    helper.waitUntilReady(commonElements.multiEmailField);
    commonElements.multiEmailField.sendKeys(commonElements.randomEmail);

    helper.waitUntilReady(commonElements.multiPhonenumberField);
    commonElements.multiPhonenumberField.sendKeys('7865551234');

    helper.waitUntilReady(commonElements.multinextGuest);
    commonElements.multinextGuest.click();

    //Guest 2
    browser.sleep(3000);
    helper.waitUntilReady(commonElements.multifirstnameField2);
    commonElements.multifirstnameField2.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.multilastnameField2);
    commonElements.multilastnameField2.sendKeys(commonElements.randomLastName);

    element(by.css('select[name="month_2"]')).element(by.cssContainingText('option', 'June')).click();
    element(by.css('select[name="day_2"]')).element(by.cssContainingText('option', '10')).click();
    element(by.css('select[name="year_2"]')).element(by.cssContainingText('option', '1983')).click();

    //browser.executeScript('window.scrollTo(0,300);');
    element(by.css('select[name="gender_2"]')).element(by.cssContainingText('option', 'Female')).click();
    //element(by.css('select[name="countryCitizenship_1"]')).element(by.cssContainingText('option', 'USA')).click();
    element(by.css('select[name="residents_2"]')).element(by.cssContainingText('option', 'FLORIDA')).click();

    helper.waitUntilReady(commonElements.continueGuestMulti);
    commonElements.continueGuestMulti.click();
    browser.sleep(2000);

    //Room2
    //Guest 3
    helper.waitUntilReady(commonElements.multiFirstnameField);
    commonElements.multiFirstnameField.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.multiLastnameField);
    commonElements.multiLastnameField.sendKeys(commonElements.randomLastName);

    element(by.css('select[name="month_1"]')).element(by.cssContainingText('option', 'March')).click();
    element(by.css('select[name="day_1"]')).element(by.cssContainingText('option', '06')).click();
    element(by.css('select[name="year_1"]')).element(by.cssContainingText('option', '1984')).click();
    element(by.css('select[name="gender_1"]')).element(by.cssContainingText('option', 'Male')).click();
    element(by.css('select[name="residents_1"]')).element(by.cssContainingText('option', 'FLORIDA')).click();

    helper.waitUntilReady(commonElements.multiEmailField);
    commonElements.multiEmailField.sendKeys(commonElements.randomEmail);

    helper.waitUntilReady(commonElements.multiPhonenumberField);
    commonElements.multiPhonenumberField.sendKeys('7865551234');

    helper.waitUntilReady(commonElements.multinextGuest);
    commonElements.multinextGuest2.click();
    browser.sleep(3000);

    //Guest 4
    helper.waitUntilReady(commonElements.multifirstnameField2);
    commonElements.multifirstnameField2.sendKeys(commonElements.randomFirstName);

    helper.waitUntilReady(commonElements.multilastnameField2);
    commonElements.multilastnameField2.sendKeys(commonElements.randomLastName);

    element(by.css('select[name="month_2"]')).element(by.cssContainingText('option', 'June')).click();
    element(by.css('select[name="day_2"]')).element(by.cssContainingText('option', '10')).click();
    element(by.css('select[name="year_2"]')).element(by.cssContainingText('option', '1983')).click();
    element(by.css('select[name="gender_2"]')).element(by.cssContainingText('option', 'Female')).click();
    element(by.css('select[name="residents_2"]')).element(by.cssContainingText('option', 'FLORIDA')).click();

    helper.waitUntilReady(commonElements.continueGuestMulti);
    commonElements.continueGuestMulti.click();

    browser.sleep(4000);
    helper.waitUntilReady(commonElements.noPPGR_Multi);
    commonElements.noPPGR_Multi.click();
    browser.sleep(4000);
    helper.waitUntilReady(commonElements.continuePref_Multi);
    commonElements.continuePref_Multi.click();
    browser.sleep(4000);
    helper.waitUntilReady(commonElements.noPPGR_Multi);
    commonElements.noPPGR_Multi.click();
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.continuePref_Multi);
    commonElements.continuePref_Multi.click();
    browser.sleep(2000);
  }


  //Restart flow
  this.restartFlow = function() {
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.changeItinerary);
    commonElements.changeItinerary.click();
    helper.waitUntilReady(commonElements.understandButton);
    commonElements.understandButton.click();
  }
  //Restart flow--Multi
  this.restartFlow_Multi = function() {
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.changeItineraryMulti);
    commonElements.changeItineraryMulti.click();
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.understandButton_Multi);
    commonElements.understandButton_Multi.click();
  }

  //Go back to Homepage
  this.returnHomepage = function() {
    browser.sleep(2000);
    helper.waitUntilReady(commonElements.logoButton);
    commonElements.logoButton.click();
    helper.waitUntilReady(commonElements.understandButton);
    commonElements.understandButton.click();
  }

  //Test CreditCard
  this.makePayment = function() {
    helper.waitUntilReady(commonElements.creditcardFirstName);
    commonElements.creditcardFirstName.sendKeys(commonElements.randomFirstName);
    commonElements.creditcardLastName.sendKeys(commonElements.randomLastName);
    commonElements.creditcardNumber.sendKeys('4387751111111111');
    element(by.id('month_1')).element(by.cssContainingText('option', 'December')).click();
    element(by.id('year_1')).element(by.cssContainingText('option', '2020')).click();
    browser.sleep(4000);
    commonElements.acceptTAC.click();
    browser.sleep(4000);
    commonElements.submitPayment.click();
    browser.sleep(4000);
  }
}



module.exports = CommonFunctions;
