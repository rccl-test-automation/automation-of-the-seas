var helper = require('../../helper.js');
var faker =require('faker');
var a = Math.floor(Math.random() * 10)+1;
var b = Math.floor(Math.random() * 2)+1;



var SpaElements = function(){
  //css

  this.newbookNow2 =element(by.css('[data-selector="search-cards-'+a+'"]'));
  this.newbookNow =element(by.css('[data-selector="search-cards-1"]'));
  this.selectDate =element(by.css('[data-selector="search-panel-date1"]'));
  //this.selectDate =element(by.css('a[class*="sailing ng-tns-c2-1"]'));
  //this.dateFilter =element(by.id('md-button-toggle-21-input'));
  this.login =element(by.xpath('/html/body/div[1]/div[3]/header/nav/div/div/nav/div[3]/div[1]'));
  this.loginButton = element(by.xpath('//*[@id="rcl-submenu-logout"]/div/div[2]/a'));
  this.productView = element(by.css('body > c:set > app-root > md-sidenav-container > md-sidenav > app-itinerary-panel > main > section.itinerary-details > button > div.mat-button-ripple.mat-ripple'));
  this.randomRoom =element(by.css('[data-selector="superCategory-'+stateroomRan()+'"]'));
  this.bookNow =element(by.css('#search-results > div:nth-child(1) > div.search-result-top > div.columns.small-5.medium-4.payment-box-col > div.booking-btn-wrap > a' ));
  this.continueStateroom = element(by.css('[data-selector="stateroom-continue"]'));
  this.occupancycontinue =element(by.css('[data-selector="occupancy-continue"]'));
  this.interiorRoom =element(by.css('[data-selector="superCategory-interior"]'));
  this.oceanRoom =element(by.css('[data-selector="superCategory-ocean-view"]'));
  this.outsideRoom =element(by.css('[data-selector="superCategory-outside-view"]'));
  this.balconyRoom =element(by.css('[data-selector="superCategory-balcony"]'));
  this.suiteRoom =element(by.css('[data-selector="superCategory-suites"]'));
  this.nrpStep =element(by.css('[data-selector="fare-option-'+b+'"]'));
  this.No_nrpStep =element(by.css('[data-selector="fare-option-2"]'));
  this.plusButton =element(by.css('[data-selector="stateroom-plus"]'));
  this.minusButton =element(by.css('[data-selector="stateroom-minus"]'));

  this.subSelection =element(by.css('[data-selector="category-category-position-1"]'));
  this.viewDetails =element(by.css('[data-selector="category-viewDetails-position-1"]'));
  this.hideDetails =element(by.css('[data-selector="category-hideDetails-position-1"]'));
  this.deckSelection =element(by.css('[data-selector="deck-continue"]'));
  this.shipSelection =element(by.css('[data-selector="location-continue"]'));
  this.cabinSelection =element(by.css('[data-selector="cabin-continue"]'));

  //this.loginButton =element(by.css('[data-selector="stateroom-continue"]'));

  //AEM Pages
  this.experienceButton =element(by.css('a[href*="#rcl-submenu-1"]'));
  this.shipsButton =element(by.css('a[href*="/cruise-ships/"]'));
  this.harmonyShip =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas"]'));
  this.harmonyThingstoDo =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.harmonyShipdecks =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/deck-plans"]'));
  this.harmonyRooms =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/rooms"]'));
  this.harmonyAwards =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/awards"]'));
  this.oasisShip =element(by.css('a[href*="/cruise-ships/oasis-of-the-seas"]'));
  this.oasisThingstoDo =element(by.css('a[href*="/cruise-ships/oasis-of-the-seas/things-to-do"]'));
  this.oasisShipdecks =element(by.css('a[href*="/cruise-ships/oasis-of-the-seas/deck-plans"]'));
  this.oasisRooms =element(by.css('a[href*="/cruise-ships/oasis-of-the-seas/rooms"]'));
  this.oasisAwards =element(by.css('a[href*="/cruise-ships/oasis-of-the-seas/awards"]'));
  this.allureShip =element(by.css('a[href*="/cruise-ships/allure-of-the-seas"]'));
  this.allureThingstoDo =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.allureShipdecks =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.allureRooms =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.allureAwards =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.anthemShip =element(by.css('a[href*="/cruise-ships/anthem-of-the-seas"]'));
  this.anthemThingstoDo =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.anthemShipdecks =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.anthemRooms =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));
  this.anthemAwards =element(by.css('a[href*="/cruise-ships/harmony-of-the-seas/things-to-do"]'));

  //Cruise Search
  this.desFilter =element(by.css('[data-selector="search-any-destination"]'));
  this.portFilter =element(by.css('[data-selector="search-any-destination"]'));
  this.dateFilter =element(by.css('[data-selector="search-any-destination"]'));


  //id
  this.creditcardNumber =element(by.id('creditCardNumber_1'));
  this.creditcardFirstName =element(by.id('firstName_1'));
  this.creditcardLastName =element(by.id('lastName_1'));


}

module.exports = SpaElements;

//Randomizers
function stateroomRan() {
var d = Math.floor(Math.random() * 3); // returns a number between 0 and 2
switch(d) {
    case 0:
        return 'interior';
        break;
    case 1:
        return 'balcony';
        break;
    case 2:
        return 'suites';
};
};
