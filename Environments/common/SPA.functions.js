var helper = require('../../helper.js');
var faker =require('faker');
var SpaElements = require('../common/SPA.objects.js');
var spaElements = new SpaElements();
var c = Math.floor(Math.random() * 3);


var SpaFunctions = function(){

  //functions

  //Old Cruise Search Flow
    this.oldCruisesearch = function (){

      helper.waitElementToBeClickable(spaElements.bookNow);
      spaElements.bookNow.click();
      console.log();
      browser.sleep(2000);
  }

  //New Cruise Search
    this.guaranteeCruiseSearch = function (){

      helper.waitElementToBeClickable(spaElements.newbookNow);
      spaElements.newbookNow.click();
      //browser.sleep(3000);
      helper.waitElementToBeClickable(spaElements.selectDate);

      spaElements.selectDate.click();
  }
  //New Cruise Search
    this.newCruiseSearch = function (){

      helper.waitUntilReady(spaElements.newbookNow2);
      spaElements.newbookNow2.click();
      browser.sleep(3000);
      helper.waitElementToBeClickable(spaElements.selectDate);
      spaElements.selectDate.click();
  }

//------------------------------------------------------------------------------------------------------------------------------------------------------------

  //AEM Pages-Ships
  this.shipPage = function (){
    browser.sleep(2000);
    helper.waitUntilReady(spaElements.experienceButton);
    spaElements.experienceButton.click();
    browser.sleep(2000);
    helper.waitUntilReady(spaElements.shipsButton);
    spaElements.shipsButton.click();
}
  //AEM Ships-Harmony
  this.harmonyPage = function (){

    browser.executeScript('window.scrollTo(0,300);');
    helper.waitUntilReady(spaElements.harmonyShip);
    browser.sleep(2000);
    spaElements.harmonyShip.click();
    }
  this.harmonyThings = function (){
    helper.waitUntilReady(spaElements.harmonyThingstoDo);
    browser.sleep(2000);
    spaElements.harmonyThingstoDo.click();
    }
  this.harmonyDecks = function (){
    helper.waitUntilReady(spaElements.harmonyShipdecks);
    browser.sleep(2000);
    spaElements.harmonyShipdecks.click();
    }
  this.harmonyRooms = function (){
    helper.waitUntilReady(spaElements.harmonyRooms);
    browser.sleep(2000);
    spaElements.harmonyRooms.click();
    }
  this.harmonyAwards = function (){
    helper.waitUntilReady(spaElements.harmonyAwards);
    browser.sleep(2000);
    spaElements.harmonyAwards.click();
    }

  //AEM Ships-Oasis
  this.oasisPage = function (){

    helper.waitUntilReady(spaElements.harmonyAwards);
    spaElements.oasisShip.click();
    }

  //AEM Ships-Allure
  this.allurePage = function (){

    helper.waitUntilReady(spaElements.allureShip);
    spaElements.allureShip.click();
    }

  //AEM Ships-Anthem
  this.anthemPage = function (){

    helper.waitUntilReady(spaElements.anthemShip);
    spaElements.anthemShip.click();
    }

//------------------------------------------------------------------------------------------------------------------------------------------------------------

  //Booking Flow Interior
  this.interiorFlow = function(){
    helper.waitElementToBeClickable(spaElements.continueStateroom);
    spaElements.continueStateroom.click();
    browser.sleep(2000);

    helper.waitElementToBeClickable(spaElements.occupancycontinue);
    spaElements.occupancycontinue.click();
    browser.sleep(2000);

    helper.waitElementToBeClickable(spaElements.interiorRoom);
    browser.sleep(2000);
    spaElements.randomRoom.click();;

    helper.waitElementToBeClickable(spaElements.nrpStep);
    browser.sleep(2000);
    spaElements.nrpStep.click();

    helper.waitElementToBeClickable(spaElements.subSelection);
    spaElements.viewDetails.click();
    browser.sleep(2000);
    spaElements.hideDetails.click();
    browser.sleep(2000);
    spaElements.subSelection.click();

    helper.waitElementToBeClickable(spaElements.shipSelection);
    spaElements.shipSelection.click();

    helper.waitElementToBeClickable(spaElements.deckSelection);
    spaElements.deckSelection.click();

    helper.waitElementToBeClickable(spaElements.cabinSelection);
    spaElements.cabinSelection.click();
  }
  //Booking Flow Interior guarantee
  this.interiorFlowGuarantee = function(){
    helper.waitElementToBeClickable(spaElements.continueStateroom);
    spaElements.continueStateroom.click();
    browser.sleep(2000);

    helper.waitElementToBeClickable(spaElements.occupancycontinue);
    spaElements.occupancycontinue.click();
    browser.sleep(2000);

    helper.waitElementToBeClickable(spaElements.interiorRoom);
    browser.sleep(2000);
    spaElements.interiorRoom.click();

    helper.waitElementToBeClickable(spaElements.nrpStep);
    browser.sleep(2000);
    spaElements.nrpStep.click();
  }


  //Booking Flow Interior (GLOBALS)
  this.interiorFlowGlobal = function(){
    helper.waitElementToBeClickable(spaElements.continueStateroom);
    spaElements.continueStateroom.click();
    browser.sleep(2000);

    helper.waitElementToBeClickable(spaElements.occupancycontinue);
    spaElements.occupancycontinue.click();
    browser.sleep(2000);

    helper.waitElementToBeClickable(spaElements.interiorRoom);
    browser.sleep(2000);
    spaElements.randomRoom.click();

    helper.waitElementToBeClickable(spaElements.subSelection);
    spaElements.viewDetails.click();
    browser.sleep(2000);
    spaElements.hideDetails.click();
    browser.sleep(2000);
    spaElements.subSelection.click();

    helper.waitElementToBeClickable(spaElements.shipSelection);
    spaElements.shipSelection.click();

    helper.waitElementToBeClickable(spaElements.deckSelection);
    spaElements.deckSelection.click();
    browser.sleep(2000);
    helper.waitElementToBeClickable(spaElements.cabinSelection);
    spaElements.cabinSelection.click();
    browser.sleep(2000);
  }


    //productView
     this.productView = function(){
       helper.waitUntilReady(spaElements.productView);
       spaElements.productView.click();
     }
}

module.exports = SpaFunctions;
