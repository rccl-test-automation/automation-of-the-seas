var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');
var LegacyElements = require('../common/Legacy.objects.js');
var LegacyFunctions = require('../common/Legacy.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');


var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

var spider = require('../common/GA.Prod.module.js');
var using = require('jasmine-data-provider');



// //------------------------------------------------------------------------------------------------------------------------------------------------------------


describe(' Guest Account-Redirects: ', function() {
  beforeAll(function() {
    browser.ignoreSynchronization = true;
    //browser.get(browser.params.url);
    //  eyes.open(browser, "Prod", "AEM Pages");
  });

  beforeEach(function() {
    browser.ignoreSynchronization = true;
    });

  afterAll(function() {
    //eyes.close();
    browser.manage().deleteAllCookies();
  });
// describe('Homepage: ', function() {
//   using(spider, function(data, description) {
//     it(data.Country + ' Homepage goes to Guest Account' , function() {
//       browser.get(data.Original);
//       browser.sleep(3000);
//       helper.waitUntilReady(legacyElements.login);
//       legacyElements.login.click();
//       helper.waitUntilReady(legacyElements.login2);
//       legacyElements.login2.click();
//       browser.sleep(3000);
//       expect(browser.getCurrentUrl()).toContain(data.Redirects);
//     });
//   });
// });
// describe(' Cruise Search (Secure): ', function() {
//   using(spider, function(data, description) {
//     it(data.Country + ' Cruise Search goes to Guest Account' , function() {
//       browser.get(data.CruiseSearch);
//       browser.sleep(1000);
//       browser.get(data.Original);
//       browser.sleep(1000);
//       browser.get(data.CruiseSearch);
//       helper.waitUntilReady(legacyElements.login);
//       legacyElements.login.click();
//       browser.sleep(3000);
//       expect(browser.getCurrentUrl()).toContain(data.Redirects);
//     });
//   });
// });
describe('  My Cruises: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- My Cruises redirects to Guest Account' , function() {
      browser.get(data.MyCruises);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
  });
  describe('  Cruise Planner: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Cruise Planner redirects to Guest Account' , function() {
      browser.get(data.CruisePlanner);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Itineraries: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Itineraries redirects to Guest Account' , function() {
      browser.get(data.Itineraries);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Purchased & Held Cruises: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Purchased & Held Cruises redirects to Guest Account' , function() {
      browser.get(data.Purchased_Held_Cruises);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Completed Cruises: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.CompletedCruises);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Profile: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.Profile);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  PersonalInformation: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.PersonalInformation);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  GuestInformation: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.GuestInformation);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  AddGuest: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.AddGuest);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Enroll/Activate: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.Enroll_Activate);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Crown_Anchor_Enrollment: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.Crown_Anchor_Enrollment);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Change_Username_Password: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.Change_Username_Password);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  CruisingPreferences: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Cruising Preferences redirects to Guest Account' , function() {
      browser.get(data.CruisingPreferences);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  OnboardPreferences: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- OnboardPreferences redirects to Guest Account' , function() {
      browser.get(data.OnboardPreferences);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Upgrade Preferences: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Completed Cruises redirects to Guest Account' , function() {
      browser.get(data.UpgradePreferences);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  Gift Preferences: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- GiftPreferences redirects to Guest Account' , function() {
      browser.get(data.GiftPreferences);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
describe('  MakeaPayment: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- MakeaPayment redirects to Guest Account' , function() {
      browser.get(data.MakeaPayment);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
// describe('  Reservation: ', function() {
//   using(spider, function(data, description) {
//     it(data.Country + '-- Reservation redirects to Guest Account' , function() {
//       browser.get(data.Reservation);
//       browser.sleep(3000);
//       expect(browser.getCurrentUrl()).toContain(data.Redirects);
//     });
//   });
// });
describe('  Messages: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Messages redirects to Guest Account' , function() {
      browser.get(data.Messages);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
// describe('Cas_Enrollment: ', function() {
//   using(spider, function(data, description) {
//     it(data.Country + '-- Cas_Enrollment redirects to Guest Account' , function() {
//       browser.get(data.Cas_Enrollment);
//       browser.sleep(3000);
//       expect(browser.getCurrentUrl()).toContain(data.Redirects);
//     });
//   });
// });
// describe('  Cas_Login: ', function() {
//   using(spider, function(data, description) {
//     it(data.Country + '-- Cas_Login redirects to Guest Account' , function() {
//       browser.get(data.Cas_Login);
//       browser.sleep(3000);
//       expect(browser.getCurrentUrl()).toContain(data.Redirects);
//     });
//   });
// });
describe('  Friend: ', function() {
  using(spider, function(data, description) {
    it(data.Country + '-- Messages redirects to Guest Account' , function() {
      browser.get(data.Messages);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.Redirects);
    });
  });
});
});
