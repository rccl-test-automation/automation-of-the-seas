// var helper = require('../../../helper.js');
// var CommonFunctions = require('../common/common.functions.js');
// var CommonElements = require('../common/common.objects.js');
// var SpaFunctions = require('../common/SPA.functions.js');
// var SpaElements = require('../common/SPA.objects.js');
// var LegacyElements = require('../common/Legacy.objects.js');
// var LegacyFunctions = require('../common/Legacy.functions.js');
// var MultiElements = require('../common/Multi.objects.js');
// var MultiFunctions = require('../common/Multi.functions.js');
//
//
// var spaFunctions = new SpaFunctions();
// var spaElements = new SpaElements();
// var commonFunctions = new CommonFunctions();
// var commonElements = new CommonElements();
// var legacyElements = new LegacyElements();
// var legacyFunctions = new LegacyFunctions();
// var multiElements = new MultiElements();
// var multiFunctions = new MultiFunctions();

var spider = require('../../common/CAS.Prod.module.js');
var using = require('jasmine-data-provider');



// //------------------------------------------------------------------------------------------------------------------------------------------------------------


describe('Cas GA_Redirect: ', function() {
  using(spider, function(data, description) {
    it(data.Country + ' CAS Login goes to Guest Account' , function() {
      browser.get(data.Login);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Enroll goes to Guest Account' , function() {
      browser.get(data.Enroll);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Enroll_Activate goes to Guest Account' , function() {
      browser.get(data.Enroll_Activate);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS SpecialOffers goes to Guest Account' , function() {
      browser.get(data.SpecialOffers);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS News goes to Guest Account' , function() {
      browser.get(data.News);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Smiles goes to Guest Account' , function() {
      browser.get(data.Smiles);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS InquiryOnline goes to Guest Account' , function() {
      browser.get(data.InquiryOnline);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS MemberRates goes to Guest Account' , function() {
      browser.get(data.MemberRates);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS NextCruiseBonus goes to Guest Account' , function() {
      browser.get(data.NextCruiseBonus);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS GGGSale goes to Guest Account' , function() {
      browser.get(data.GGGSale);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
  
    it(data.Country + ' CAS Programma goes to Guest Account' , function() {
      browser.get(data.Programma);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Socios goes to Guest Account' , function() {
      browser.get(data.Socios);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Beneficios_Para_Socios goes to Guest Account' , function() {
      browser.get(data.Beneficios_Para_Socios);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Home goes to Guest Account' , function() {
      browser.get(data.Home);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Member_Area goes to Guest Account' , function() {
      browser.get(data.Member_Area);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Member_Benefits goes to Guest Account' , function() {
      browser.get(data.Member_Benefits);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });

    it(data.Country + ' CAS Member_Cruises goes to Guest Account' , function() {
      browser.get(data.Member_Cruises);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Past_Member_Cruises goes to Guest Account' , function() {
      browser.get(data.Past_Member_Cruises);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Upcoming_Member_Cruises goes to Guest Account' , function() {
      browser.get(data.Upcoming_Member_Cruises);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Member_Stories goes to Guest Account' , function() {
      browser.get(data.Member_Stories);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Special_Offers goes to Guest Account' , function() {
      browser.get(data.Special_Offers);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Exclusive_Rates goes to Guest Account' , function() {
      browser.get(data.Exclusive_Rates);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Balcony_Suites_Discount goes to Guest Account' , function() {
      browser.get(data.Balcony_Suites_Discount);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Preview_Sale goes to Guest Account' , function() {
      browser.get(data.Preview_Sale);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS News_More goes to Guest Account' , function() {
      browser.get(data.News_More);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Exclusive_News goes to Guest Account' , function() {
      browser.get(data.Exclusive_News);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
    it(data.Country + ' CAS Behind_the_Smiles goes to Guest Account' , function() {
      browser.get(data.Behind_the_Smiles);
      browser.sleep(3000);
      expect(browser.getCurrentUrl()).toContain(data.GA_Redirect);
    });
  });
});
