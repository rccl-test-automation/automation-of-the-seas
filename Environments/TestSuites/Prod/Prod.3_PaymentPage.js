var helper = require('../../../helper.js');

var PaymentPage = require('../../PageObjects/Payment.js');

var paymentPage = new PaymentPage();


// var Eyes = require("eyes.selenium").Eyes;
// var eyes = new Eyes();
// eyes.setApiKey("ZFbVh1c104vva1RTdZaDH2brtHEU2j110OM6HhDAj1OzOmQ110");
// eyes.setForceFullPageScreenshot(true);

//
  describe('Pagement Page', function() {
    beforeAll(function() {
      browser.ignoreSynchronization = true;
      browser.get(browser.params.url);
      //eyes.open(browser, "Prod", "Legacy Pages");
    });

    afterAll(function() {
      browser.manage().deleteAllCookies();
    });

    it('Navigate to Payment Page', function() {
      paymentPage.navigateBooking();
      //eyes.checkWindow("Mycruise Log-in page");
    });

    it('Retrieve', function() {
      paymentPage.retrieveBooking();
    });

    // it('Payment', function() {
    //   paymentPage.retrieveBooking();
    // });
  });

//------------------------------------------------------------------------------------------------------------------------------------------------------------
