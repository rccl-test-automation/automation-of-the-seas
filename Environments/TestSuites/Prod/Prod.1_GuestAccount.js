var helper = require('../../../helper.js');
var CommonFunctions = require('../../common/common.functions.js');
var CommonElements = require('../../common/common.objects.js');
var SpaFunctions = require('../../common/SPA.functions.js');
var SpaElements = require('../../common/SPA.objects.js');
var LegacyElements = require('../../common/Legacy.objects.js');
var LegacyFunctions = require('../../common/Legacy.functions.js');
var MultiElements = require('../../common/Multi.objects.js');
var MultiFunctions = require('../../common/Multi.functions.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

// var Eyes = require("eyes.selenium").Eyes;
// var eyes = new Eyes();
// eyes.setApiKey("ZFbVh1c104vva1RTdZaDH2brtHEU2j110OM6HhDAj1OzOmQ110");
// eyes.setForceFullPageScreenshot(true);

//
  describe('Guest Account', function() {
    beforeAll(function() {
      browser.ignoreSynchronization = true;
      browser.get(browser.params.url);
      //eyes.open(browser, "Prod", "Legacy Pages");
    });

    afterAll(function() {
      browser.manage().deleteAllCookies();
    });

    it('Logging in', function() {
      legacyFunctions.hpLogin();
      //eyes.checkWindow("Mycruise Log-in page");
    });

  });

//------------------------------------------------------------------------------------------------------------------------------------------------------------
