var helper = require('../../../helper.js');
var CommonFunctions = require('../../common/common.functions.js');
var CommonElements = require('../../common/common.objects.js');
var SpaFunctions = require('../../common/SPA.functions.js');
var SpaElements = require('../../common/SPA.objects.js');
var LegacyElements = require('../../common/Legacy.objects.js');
var LegacyFunctions = require('../../common/Legacy.functions.js');
var MultiElements = require('../../common/Multi.objects.js');
var CruiseSearch = require('../../PageObjects/CruiseSearch.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var cruiseSearch = new CruiseSearch();

// var Eyes = require("eyes.selenium").Eyes;
// var eyes = new Eyes();
// eyes.setApiKey("ZFbVh1c104vva1RTdZaDH2brtHEU2j110OM6HhDAj1OzOmQ110");
// eyes.setForceFullPageScreenshot(true);

//
  describe('Legacy', function() {
    beforeAll(function() {
      browser.ignoreSynchronization = true;
      //browser.get(browser.params.url);
      //eyes.open(browser, "Prod", "Legacy Pages");
    });

    afterAll(function() {
      browser.manage().deleteAllCookies();
    });

    it('OLCI', function() {
      browser.get(browser.params.url);
      browser.sleep(2000);
      legacyFunctions.olciLogin();
      //eyes.checkWindow("OLCI");
    });

    // it('Cruise Planner', function() {
    //   browser.get('https://secure.royalcaribbean.com/asr/login.do?cS=NAVBAR&pnav=3&snav=3');
    //   legacyFunctions.cpLogin();
    //   //eyes.checkWindow("Cruise Planner");
    // });

  });

//------------------------------------------------------------------------------------------------------------------------------------------------------------
