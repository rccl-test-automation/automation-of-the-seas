var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');
var LegacyElements = require('../common/Legacy.objects.js');
var LegacyFunctions = require('../common/Legacy.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');


var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

var spider = require('../common/redirects.module.js');
var spider2 = require('../common/Brochure.module.js');
var using = require('jasmine-data-provider');



// //------------------------------------------------------------------------------------------------------------------------------------------------------------


describe(' Guest Account-Internationals: ', function() {
  beforeAll(function() {
    browser.ignoreSynchronization = true;
    //browser.get(browser.params.url);
    //  eyes.open(browser, "Prod", "AEM Pages");
  });

  beforeEach(function() {
    browser.ignoreSynchronization = true;
    });

  afterAll(function() {
    //eyes.close();
    browser.manage().deleteAllCookies();
  });


  using(spider, function(data, description) {
    it(data.Country + ' Homepage goes to Guest Account' , function() {
      browser.get(data.Original);
      helper.waitUntilReady(legacyElements.gaLogin);
      legacyElements.gaLogin.click();
      expect(browser.getCurrentUrl()).toEqual(data.GuestAccount);
      //Checking for the 404 page
      //expect($$('img[src*="http://media.royalcaribbean.com/img/emergency_home_main2.gif"]').count()).toBe(0);
      // helper.waitUntilReady(legacyElements.usernameFieldGA);
      // legacyElements.usernameFieldGA.sendKeys('edleeash3@comcast.net');
      // helper.waitUntilReady(legacyElements.passwordField);
      // legacyElements.passwordField.sendKeys('Royal123');
      // helper.waitUntilReady(legacyElements.loginButtonGA);
      // legacyElements.loginButtonGA.click();
      // expect(browser.getCurrentUrl()).toContain(data.Account);
    });
  });

  using(spider, function(data, description) {
    it(data.Country + ' From Cruise Search (secure) to Guest Account', function() {
      browser.get(data.Original);
      browser.get(data.CruiseSearch);
      browser.get(data.Original);
      helper.waitUntilReady(commonElements.searchButton);
      commonElements.searchButton.click();
      helper.waitUntilReady(legacyElements.gaLogin);
      legacyElements.gaLogin.click();
      expect(browser.getCurrentUrl()).toEqual(data.GuestAccount);
    });
  });
});
