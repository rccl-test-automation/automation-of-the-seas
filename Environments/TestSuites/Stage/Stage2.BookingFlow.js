var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');
var LegacyElements = require('../common/Legacy.objects.js');
var LegacyFunctions = require('../common/Legacy.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');


var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

var Eyes = require("eyes.selenium").Eyes;
var eyes = new Eyes();
eyes.setApiKey("MZpSn8F78CHmaeDakR2i6xqSxhwHvmU9pnbHDUz0lV4110");

describe('Legacy', function() {

  describe(' Pages: ', function() {
    beforeAll(function() {
      browser.get('https://test:royal@secure.stage2.royalcaribbean.com/cruises');
      browser.get('https://www.stage2.royalcaribbean.com');
      browser.ignoreSynchronization = true;
      browser.manage().deleteAllCookies();
      //eyes.open(browser, "Prod", "Legacy Pages");
    });

    beforeEach(function() {
      browser.ignoreSynchronization = true;
    });
    afterEach(function() {
      browser.manage().deleteAllCookies();
    });
    afterAll(function() {
      //eyes.close();
    });

    it('Guest Account', function() {
      legacyFunctions.hpLoginStage2();
      //eyes.checkWindow("Mycruise Log-in page");
    });

    // it('OLCI', function() {
    //   browser.get('https://test:royal@secure.stage2.royalcaribbean.com/cruises');
    //   browser.get('https://www.stage2.royalcaribbean.com');
    //   //browser.sleep(2000);
    //   legacyFunctions.olciLoginStage2();
    //   //eyes.checkWindow("OLCI");
    // });
    // it('Cruise Planner', function() {
    //   browser.get('https://secure.stage2.royalcaribbean.com/asr/login.do?cS=NAVBAR&pnav=3&snav=3');
    //   legacyFunctions.cpLogin();
    //   //eyes.checkWindow("Cruise Planner");
    // });
  });

});
//------------------------------------------------------------------------------------------------------------------------------------------------------------
//
describe('Booking Flow:', function() {

  beforeAll(function() {
    browser.get('https://test:royal@secure.stage2.royalcaribbean.com/cruises');
    browser.get('https://secure.stage2.royalcaribbean.com');
    browser.ignoreSynchronization = true;
    browser.manage().deleteAllCookies();
    //eyes.open(browser, "Stage2", "BAU");
  });

  beforeEach(function(){
    browser.ignoreSynchronization = true;
  });

  afterAll(function(){
    //eyes.close();
    browser.manage().deleteAllCookies();
  });

  describe('Interior Room - ', function() {
    // Entering AEM
      it('should select Search on the Homepage', function() {
        //eyes.checkWindow("Homepage");
        browser.sleep(2000);
        commonElements.searchButton.click();
      });


    //Cruise Search
      it('Homepage to Cruise Search', function(){
        //eyes.checkWindow("Cruisesearch");
        spaFunctions.newCruiseSearch();
      });

    //Starts the interior booking Flow
      it('Select 2 Guest and NRP', function() {
        spaFunctions.interiorFlow();
      });

    //Enter guest info
      it('Guest info', function() {
        commonFunctions.guestInfo();
        //eyes.checkWindow("Payment Page");
      });

    //restart the flow
      it('Restart', function() {
        commonFunctions.restartFlow();
      });
  });

  describe('Oceanview Room - ', function() {

    //Cruise Search
      it('Cruise Search', function(){
      spaFunctions.newCruiseSearch();
      });

    //Starts the interior booking Flow
      it('2 Guest and NRP', function() {
        spaFunctions.oceanviewFlow();
      });

    //Enter guest info
      it('Guest info- Logging in', function() {
        commonFunctions.guestInfoLoggedin();
      });

    //restart the flow
      it('Restart', function() {
        commonFunctions.restartFlow();
      });
  });

  describe('Suite - ', function() {

  //Cruise Search
    it('Cruise Search', function(){
    spaFunctions.newCruiseSearch();
    });

  //Starts the interior booking Flow
  it('2 Guest and no-NRP', function() {
    spaFunctions.suiteFlow();
  });

  //Enter guest info
  it('Guest info', function() {
    commonFunctions.guestInfo();
  });

  //restart the flow
  it('Restart', function() {
    commonFunctions.restartFlow();
      });
  });

    describe('MultiStateroom - ', function() {

      //Cruise Search
        it('Cruise Search', function(){
        spaFunctions.newCruiseSearch();
        });

      //Starts the interior booking Flow
        it('2 Rooms', function() {
          multiFunctions.multiRoom();
        });

      //Enter guest info
        it('Guest info', function() {
          commonFunctions.guestInfoMulti();
        });

      //restart the flow
        it('Restart', function() {
          commonFunctions.restartFlow_Multi();
        });
    });
});

//------------------------------------------------------------------------------------------------------------------------------------------------------------

describe('AEM ', function() {

  describe(' Pages: ', function() {
    beforeAll(function() {
      browser.get('https://test:royal@secure.stage2.royalcaribbean.com/cruises');
      browser.get('https://secure.stage2.royalcaribbean.com');
      browser.ignoreSynchronization = true;
      browser.manage().deleteAllCookies();
      //eyes.open(browser, "Prod", "AEM Pages");
    });

    beforeEach(function() {
      browser.ignoreSynchronization = true;
    });
    afterEach(function() {
      browser.manage().deleteAllCookies();
    });
    afterAll(function() {
      //eyes.close();
    });


    it('Ships', function() {

      spaFunctions.shipPage();
      //eyes.checkWindow("Ships Page");


    });
    it('Harmony', function() {
      spaFunctions.harmonyPage();
      //eyes.checkWindow("Harmony");
      spaFunctions.harmonyThings();
      //eyes.checkWindow("Harmony things-to-do");
      spaFunctions.harmonyDecks();
      //eyes.checkWindow("Harmony Deck Plans");
      spaFunctions.harmonyRooms();
      //eyes.checkWindow("Harmony Rooms");
      spaFunctions.harmonyAwards();
      //eyes.checkWindow("Harmony Awards");
    });

  });
});
