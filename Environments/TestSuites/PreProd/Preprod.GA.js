var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');
var LegacyElements = require('../common/Legacy.objects.js');
var LegacyFunctions = require('../common/Legacy.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');


var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

var spider = require('../common/GA.Preprod.module.js');
var using = require('jasmine-data-provider');



// //------------------------------------------------------------------------------------------------------------------------------------------------------------


describe('GA', function() {
  beforeAll(function() {
    browser.ignoreSynchronization = true;
  });

  beforeEach(function() {
    browser.ignoreSynchronization = true;
  });

  afterEach(function() {
    browser.manage().deleteAllCookies();
  });
  describe('Homepage: ', function() {
    using(spider, function(data, description) {
      it(data.Country + ' Homepage goes to Guest Account', function() {
        browser.get(data.Original);
        browser.sleep(4000);
        helper.waitUntilReady(legacyElements.login);
        legacyElements.login.click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toEqual(data.Redirects);
      });
    });
  });
  describe('Logging into Guest Account: ', function() {
    it('Log into Guest Account from Homepage', function() {
      browser.get('https://www.royalcaribbean.com/?wuc=USA');
      browser.sleep(4000);
      helper.waitUntilReady(legacyElements.login);
      legacyElements.login.click();
      helper.waitUntilReady(legacyElements.usernameFieldGA);
      legacyElements.usernameFieldGA.sendKeys('jasontur@rccl.com');
      helper.waitUntilReady(legacyElements.passwordField);
      legacyElements.passwordField.sendKeys('password1');
      helper.waitUntilReady(legacyElements.loginButtonGA);
      legacyElements.loginButtonGA.click();
      helper.waitUntilReady(legacyElements.gaName);
      expect(legacyElements.gaName.getText()).toBe('Kristin');
      expect(browser.getCurrentUrl()).toEqual('https://www.new.royalcaribbean.com/account/');
    });
  });
  describe('CS', function() {
    using(spider, function(data, description) {
      it(data.Country + ' Cruise Search goes to Guest Account', function() {
        browser.get(data.CruiseSearch);
        browser.sleep(1000);
        browser.get(data.Original);
        browser.sleep(1000);
        browser.get(data.CruiseSearch);
        helper.waitUntilReady(legacyElements.login);
        legacyElements.login.click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toEqual(data.Redirects);
      });
    });
  });
  describe('NewAccount', function() {
    using(spider, function(data, description) {
      it(data.Country + 'Create a new account redirects to Guest Account', function() {
        browser.get(data.MyCruises);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toEqual(data.CreateRedirect);
      });
    });
  });
  describe('  My Cruises: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- My Cruises redirects to Guest Account', function() {
        browser.get(data.MyCruises);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Cruise Planner: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Cruise Planner redirects to Guest Account', function() {
        browser.get(data.CruisePlanner);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Itineraries: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Itineraries redirects to Guest Account', function() {
        browser.get(data.Itineraries);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Purchased & Held Cruises: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Purchased & Held Cruises redirects to Guest Account', function() {
        browser.get(data.Purchased_Held_Cruises);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Completed Cruises: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.CompletedCruises);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Profile: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.Profile);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  PersonalInformation: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.PersonalInformation);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  GuestInformation: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.GuestInformation);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  AddGuest: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.AddGuest);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Enroll/Activate: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.Enroll_Activate);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Crown_Anchor_Enrollment: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.Crown_Anchor_Enrollment);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Change_Username_Password: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.Change_Username_Password);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  CruisingPreferences: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Cruising Preferences redirects to Guest Account', function() {
        browser.get(data.CruisingPreferences);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  OnboardPreferences: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- OnboardPreferences redirects to Guest Account', function() {
        browser.get(data.OnboardPreferences);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Upgrade Preferences: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Completed Cruises redirects to Guest Account', function() {
        browser.get(data.UpgradePreferences);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Gift Preferences: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Gift Preferences redirects to Guest Account', function() {
        browser.get(data.GiftPreferences);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Make a Payment: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Make a Payment redirects to Guest Account', function() {
        browser.get(data.MakeaPayment);
        browser.sleep(4000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Reservation: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Reservation redirects to Guest Account', function() {
        browser.get(data.Reservation);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Messages: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Messages redirects to Guest Account', function() {
        browser.get(data.Messages);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
  describe('  Friend: ', function() {
    using(spider, function(data, description) {
      it(data.Country + '-- Messages redirects to Guest Account', function() {
        browser.get(data.Messages);
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(data.Redirects);
      });
    });
  });
});
