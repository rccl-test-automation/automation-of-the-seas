var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');
var LegacyElements = require('../common/Legacy.objects.js');
var LegacyFunctions = require('../common/Legacy.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

var Eyes = require("eyes.selenium").Eyes;
var eyes = new Eyes();
eyes.setApiKey("MZpSn8F78CHmaeDakR2i6xqSxhwHvmU9pnbHDUz0lV4110");

// describe('AEM ', function() {
//
//   describe(' Pages: ', function() {
//     beforeAll(function() {
//         browser.ignoreSynchronization = true;
//         eyes.open(browser, "Prod", "AEM");
//       });
//
//       beforeEach(function(){
//         browser.ignoreSynchronization = true;
//       });
//       afterEach(function(){
//           browser.manage().deleteAllCookies();
//         });
//
//       afterAll(function(){
//           eyes.close();
//         });
//
//
//     it('Ships', function() {
//       spaFunctions.shipPage();
//       eyes.checkWindow("Ships");
//        });
//
//     it('Harmony', function() {
//         spaFunctions.harmonyPage();
//         eyes.checkWindow("Harmony");
//         spaFunctions.harmonyThings();
//         eyes.checkWindow("Things-to-do");
//         spaFunctions.harmonyDecks();
//         eyes.checkWindow("Decks");
//         spaFunctions.harmonyRooms();
//         eyes.checkWindow("Rooms");
//         spaFunctions.harmonyAwards();
//         eyes.checkWindow("Awards");
//         });
//       });
//   });

//------------------------------------------------------------------------------------------------------------------------------------------------------------

// describe('Legacy', function() {
//
//   describe(' Pages: ', function() {
//     beforeAll(function() {
//       browser.get('https://www.new.royalcaribbean.com/');
//       //eyes.open(browser, "Prod", "AEM");
//     });
//
//     beforeEach(function() {
//       browser.ignoreSynchronization = true;
//       browser.get('https://www.new.royalcaribbean.com/');
//     });
//     afterEach(function() {
//       browser.manage().deleteAllCookies();
//     });
//     afterAll(function() {
//       //eyes.close();
//     });
//     it('Cruise Planner', function() {
//       browser.get('https://secure.new.royalcaribbean.com/asr/login.do?cS=NAVBAR&pnav=3&snav=3');
//       legacyFunctions.cpLogin();
//     });
//
//     it('MyCruises', function() {
//       legacyFunctions.hpLoginPreprod();
//       //eyes.checkWindow("Mycruise Log-in page");
//     });
//
//     it('CAS', function() {
//       legacyFunctions.casLoginPreprod();
//       //eyes.checkWindow("CAS");
//     });
//
//     it('OLCI', function() {
//       legacyFunctions.olciLoginPreprod();
//     });
//   });
// });

//------------------------------------------------------------------------------------------------------------------------------------------------------------

describe('Booking Flow:', function() {

  beforeAll(function() {
    browser.ignoreSynchronization = true;
    browser.get('https://test:royal@secure.new.royalcaribbean.com/cruises');
    browser.get('https://secure.new.royalcaribbean.com');
    //eyes.open(browser, "Prod", "BAU");
  });

  beforeEach(function() {
    browser.ignoreSynchronization = true;
  });

  afterAll(function() {
    //eyes.close();
    browser.manage().deleteAllCookies();
  });

  describe('Interior Room - ', function() {
    // Entering AEM
    it('should select Search on the Homepage', function() {
      //eyes.checkWindow("Homepage");
      commonElements.searchButton.click();
    });


    //Cruise Search
    it('Homepage to Cruise Search', function() {
      //eyes.checkWindow("Cruisesearch");
      spaFunctions.newCruiseSearch();
    });

    //Starts the interior booking Flow
    it('Select 2 Guest and NRP', function() {
      spaFunctions.interiorFlow();
    });

    //Enter guest info
    it('Guest info', function() {
      commonFunctions.guestInfo();
      //eyes.checkWindow("Payment Page");
    });

    //restart the flow
    it('Restart', function() {
      commonFunctions.restartFlow();
    });
  });

  // describe('Oceanview Room - ', function() {
  //
  //   //Cruise Search
  //   it('Homepage to Cruise Search', function() {
  //     spaFunctions.newCruiseSearch();
  //   });
  //
  //   //Starts the Oceanview booking Flow
  //   it('2 Guest and NRP', function() {
  //     spaFunctions.oceanviewFlow();
  //   });
  //
  //   //Enter guest info
  //   it('Guest info', function() {
  //     commonFunctions.guestInfo();
  //   });
  //
  //   //restart the flow
  //   it('Restart', function() {
  //     commonFunctions.restartFlow();
  //     //spaFunctions.newCruiseSearch();
  //   });
  // });
  //
  // describe('Suite - ', function() {
  //
  //   //Cruise Search
  //   it('should select a cruise', function() {
  //     spaFunctions.newCruiseSearch();
  //   });
  //
  //   //Starts the interior booking Flow
  //   it('2 Guest and no-NRP', function() {
  //     spaFunctions.suiteFlow();
  //   });
  //
  //   //Enter guest info
  //   it('Guest info', function() {
  //     commonFunctions.guestInfo();
  //   });
  //
  //   //restart the flow
  //   it('Restart', function() {
  //     commonFunctions.restartFlow();
  //     //spaFunctions.newCruiseSearch();
  //   });
    describe('MultiStateroom - ', function() {

      //Cruise Search
        it('Cruise Search', function(){
        spaFunctions.newCruiseSearch();
        });

      //Starts the interior booking Flow
        it('2 Rooms', function() {
          multiFunctions.multiRoom();
        });

      //Enter guest info
        it('Guest info', function() {
          commonFunctions.guestInfoMulti();
        });

      //restart the flow
        it('Restart', function() {
          commonFunctions.restartFlow_Multi();
        });
    });
      });
