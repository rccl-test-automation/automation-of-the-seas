var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
describe('A user booking a cruise', function() {

  beforeAll(function() {
    browser.ignoreSynchronization = true;
    //browser.get(browser.params.url);
    browser.get('https://test:royal@secure.test1.royalcaribbean.com/cruises');
    browser.get('https://secure.test1.royalcaribbean.com');
  });

  afterAll(function(){
    browser.manage().deleteAllCookies();
  });

  // Entering AEM
  it('should select Search on the Homepage', function() {
    commonElements.searchButton.click();
  });

    //Cruise Search Selection
    it( 'should pick the first cruise' , function() {
      spaFunctions.newCruiseSearch();
    });

    //Starts the interior booking Flow
    it('should go thru the interior room booking flow', function() {
      spaFunctions.oceanviewFlow();
    });

    //Enter guest info
    it('should enter the guest info and make a payment then restart the flow', function() {
      commonFunctions.guestInfo();
      commonFunctions.makePayment();
      browser.sleep(2000);
      commonFunctions.restartFlow();
      browser.sleep(2000);
    });

    //
    // //Starts the oceanview booking Flow
    // it('should select an oceanview room and continue thru the booking flow', function() {
    //   spaElements.oceanviewFlow();
    // });
    //
    // //Enter guest info
    // it('should enter the guest info', function() {
    //   commonElements.guestInfo();
    // });
    //
    // //restart the flow
    // it('should restart the flow', function() {
    //   commonElements.restartFlow();
    //   spaElements.newCruiseSearchStage2();
    // });
    //
    // //Starts the suite booking Flow
    // it('should select an oceanview room and continue thru the booking flow', function() {
    //   spaElements.suiteFlow();
    // });
    //
    // //Enter guest info
    // it('should enter the guest info', function() {
    //   commonElements.guestInfo();
    // });
    //
    // //restart the flow
    // it('should restart the flow', function() {
    //   commonElements.restartFlow();
    // });

  });
