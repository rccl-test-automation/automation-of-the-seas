var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var multiFunctions = new MultiFunctions();
var multiElements = new MultiElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();

describe('A user booking a cruise with 2 rooms', function() {

  beforeAll(function() {
    //browser.get(browser.params.url);
    browser.ignoreSynchronization = true;
    //browser.get(browser.params.url);
    browser.get('https://rclgdp-test3.elasticbeanstalk.com/cruises/');
  });

  beforeEach(function(){
    browser.ignoreSynchronization = true;
  });

  afterAll(function(){
    browser.manage().deleteAllCookies();
  });

  // // Entering AEM
  // it('should select Search on the Homepage', function() {
  //   commonElements.searchButton.click();
  // });

  //Cruise Search
    it('should select a cruise', function(){
    multiFunctions.newCruiseSearchTst3();
    });

  //Starts the interior booking Flow
  it('should select an interior room and continue thru the booking flow', function() {
    helper.waitUntilReady(multiElements.roomPlus);
    multiElements.roomPlus.click();

    helper.waitUntilReady(multiElements.continueStateroom);
    multiElements.continueStateroom.click();

    browser.sleep(4000);
    browser.executeScript('window.scrollTo(0,200);');
    helper.waitUntilReady(multiElements.oceanRoom);
    multiElements.oceanRoom.click();

    browser.sleep(5000);
    browser.executeScript('window.scrollTo(0,200);');
    helper.waitUntilReady(multiElements.interiorRoom);
    multiElements.interiorRoom.click();

    helper.waitUntilReady(multiElements.multiRoomContinue);
    multiElements.multiRoomContinue.click();


  });

  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonFunctions.guestInfoPT();
  // });

  //restart the flow
  it('should restart the flow', function() {
    commonFunctions.restartFlowPT();
    multiFunctions.newCruiseSearchTst3();
  });


  //Starts the oceanview booking Flow
  it('should select an oceanview room and continue thru the booking flow', function() {
    spaFunctions.oceanviewFlow();
  });

  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonFunctions.guestInfo();
  // });
  //
  //restart the flow
  it('should restart the flow', function() {
    commonFunctions.restartFlow();
    spaFunctions.newCruiseSearch();
  });
  //
  // // //Starts the Balcony booking Flow
  // // it('should go thru the booking flow', function() {
  // //   spaElements.balconyFlow();
  // // });
  // //
  // // //Enter guest info
  // // it('should enter the guest info', function() {
  // //   commonElements.guestInfo();
  // // });
  // //
  // // //restart the flow
  // // it('should restart the flow', function() {
  // //   commonElements.restartFlow();
  // //   spaElements.newCruiseSearch();
  // // });
  //
  // //Starts the suite booking Flow
  // it('should select an suite and continue thru the booking flow', function() {
  //   spaFunctions.suiteFlow();
  // });
  //
  // //Enter guest info
  // it('should enter the guest info', function() {
  //   commonFunctions.guestInfo();
  // });
  //
  // //restart the flow
  // it('should restart the flow', function() {
  //   commonFunctions.restartFlow();
  // });

});
