var helper = require('../../helper.js');
var CommonFunctions = require('../common/common.functions.js');
var CommonElements = require('../common/common.objects.js');
var SpaFunctions = require('../common/SPA.functions.js');
var SpaElements = require('../common/SPA.objects.js');
var LegacyElements = require('../common/Legacy.objects.js');
var LegacyFunctions = require('../common/Legacy.functions.js');
var MultiElements = require('../common/Multi.objects.js');
var MultiFunctions = require('../common/Multi.functions.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();

// var Eyes = require("eyes.selenium").Eyes;
// var eyes = new Eyes();
// eyes.setApiKey("ZFbVh1c104vva1RTdZaDH2brtHEU2j110OM6HhDAj1OzOmQ110");
// eyes.setForceFullPageScreenshot(true);

//
  describe('Legacy & Guest Account', function() {
    beforeAll(function() {
      browser.ignoreSynchronization = true;
      browser.get(browser.params.url);
      //eyes.open(browser, "Prod", "Legacy Pages");
    });

    afterAll(function() {
      browser.manage().deleteAllCookies();
    });

    it('MyCruises', function() {
      legacyFunctions.hpLogin();
      //eyes.checkWindow("Mycruise Log-in page");
    });

    it('OLCI', function() {
      browser.get(browser.params.url);
      browser.sleep(2000);
      legacyFunctions.olciLogin();

       
      //eyes.checkWindow("OLCI");
    });
    it('Cruise Planner', function() {
      browser.get('https://secure.royalcaribbean.com/asr/login.do?cS=NAVBAR&pnav=3&snav=3');
      legacyFunctions.cpLogin();
      //eyes.checkWindow("Cruise Planner");
    });
  });

//------------------------------------------------------------------------------------------------------------------------------------------------------------


  describe('(USA)Interior Room - ', function() {
    // Entering AEM
    beforeAll(function() {
      browser.ignoreSynchronization = true;
      browser.get(browser.params.url);
      //eyes.open(browser, "Prod", "BAU");
    });

     afterAll(function(){
      //eyes.close();
      browser.manage().deleteAllCookies();
    });

      it('Homepage', function() {
        //eyes.checkWindow("Homepage");
        browser.sleep(2000);
        commonElements.searchButton.click();
      });


    //Cruise Search
      it('Cruise Search', function(){
        //eyes.checkWindow("Cruisesearch");
        browser.sleep(2000);
        spaFunctions.newCruiseSearch();
      });

    //Starts the interior booking Flow
      it('Select 2 Guest and NRP', function() {
        spaFunctions.interiorFlow();
      });

    //Enter guest info
      it('Guest info', function() {
        commonFunctions.guestInfo();
        //eyes.checkWindow("Payment Page");
      });

    //restart the flow
      it('Restart', function() {
        commonFunctions.restartFlow();
      });
  });

    describe('(USA) MultiStateroom - ', function() {
      beforeAll(function() {
        browser.ignoreSynchronization = true;
        //eyes.open(browser, "Prod", "BAU");
      });

       afterAll(function(){
        //eyes.close();
        browser.manage().deleteAllCookies();
      });
      //Cruise Search
        it('Cruise Search', function(){
        spaFunctions.newCruiseSearch();
        });

      //Starts the interior booking Flow
        it('2 Rooms', function() {
          multiFunctions.multiRoom();
        });

      //Enter guest info
        it('Guest info', function() {
          commonFunctions.guestInfoMulti();
        });

      //restart the flow
        it('Restart', function() {
          commonFunctions.restartFlow_Multi();
        });
    });

    describe('(Spain) Interior Room - ', function() {
      // Entering AEM
      beforeAll(function() {
        browser.ignoreSynchronization = true;
        browser.get('https://www.royalcaribbean.es/?wuc=ESP');
        //eyes.open(browser, "Prod", "BAU");
      });

       afterAll(function(){
        //eyes.close();
        browser.manage().deleteAllCookies();
      });

        it('Homepage', function() {
          //eyes.checkWindow("Homepage");
          browser.sleep(2000);
          commonElements.searchButton.click();
        });


      //Cruise Search
        it('Cruise Search', function(){
          //eyes.checkWindow("Cruisesearch");
          spaFunctions.newCruiseSearch();
        });

      //Starts the interior booking Flow
        it('Select 2 Guest and an Interior Room', function() {
          spaFunctions.interiorFlowGlobal();
        });

      //Enter guest info
        it('Guest info', function() {
          commonFunctions.guestInfoSpanish();
          //eyes.checkWindow("Payment Page");
        });

      //restart the flow
        it('Restart', function() {
          commonFunctions.restartFlow();
        });
    });


    describe('(LACAR) Interior Room - ', function() {
      // Entering AEM
      beforeAll(function() {
        browser.ignoreSynchronization = true;
        browser.get('https://www.royalcaribbean-espanol.com/mex.html?wuc=MEX');
        //eyes.open(browser, "Prod", "BAU");
      });

       afterAll(function(){
        //eyes.close();
        browser.manage().deleteAllCookies();
      });

        it('Homepage', function() {
          //eyes.checkWindow("Homepage");
          browser.sleep(2000);
          commonElements.searchButton.click();
        });


      //Cruise Search
        it('Cruise Search', function(){
          //eyes.checkWindow("Cruisesearch");
          spaFunctions.newCruiseSearch();
        });

      //Starts the interior booking Flow
        it('Select 2 Guest and an Interior Room', function() {
          spaFunctions.interiorFlowGlobal();
        });

      //Enter guest info
        it('Guest info', function() {
          commonFunctions.guestInfoSpanish();
          //eyes.checkWindow("Payment Page");
        });
    });

    // describe('(Australia) Interior Room - ', function() {
    //   // Entering AEM
    //   beforeAll(function() {
    //     browser.ignoreSynchronization = true;
    //     browser.get('https://www.royalcaribbean.com.au/');
    //     //eyes.open(browser, "Prod", "BAU");
    //   });
    //
    //    afterAll(function(){
    //     //eyes.close();
    //     browser.manage().deleteAllCookies();
    //   });
    //
    //     it('Homepage', function() {
    //       //eyes.checkWindow("Homepage");
    //       commonElements.searchButton.click();
    //     });
    //
    //
    //   //Cruise Search
    //     it('Cruise Search', function(){
    //       //eyes.checkWindow("Cruisesearch");
    //       spaFunctions.newCruiseSearch();
    //     });
    //
    //   //Starts the interior booking Flow
    //     it('Select 2 Guest and an Interior Room', function() {
    //       spaFunctions.interiorFlowGlobal();
    //     });
    //
    //   //Enter guest info
    //     it('Guest info', function() {
    //       commonFunctions.guestInfoAUS();
    //       //eyes.checkWindow("Payment Page");
    //     });
    //
    //   //restart the flow
    //     it('Restart', function() {
    //       commonFunctions.restartFlow();
    //     });
    // });

    describe('(Brasil) Interior Room - ', function() {
      // Entering AEM
      beforeAll(function() {
        browser.ignoreSynchronization = true;
        browser.get('https://www.royalcaribbean.com.br/?wuc=BRA');
        //eyes.open(browser, "Prod", "BAU");
      });

       afterAll(function(){
        //eyes.close();
        browser.manage().deleteAllCookies();
      });

        it('Homepage', function() {
          //eyes.checkWindow("Homepage");
          browser.sleep(2000);
          commonElements.searchButton.click();
        });


      //Cruise Search
        it('Cruise Search', function(){
          //eyes.checkWindow("Cruisesearch");
          spaFunctions.oldCruisesearch();
        });

      //Starts the interior booking Flow
        it('Select 2 Guest', function() {
          multiFunctions.GDP();
        });

      //Enter guest info
        it('Guest info', function() {
          commonFunctions.guestInfoBRA();
          //eyes.checkWindow("Payment Page");
        });

      //restart the flow
        it('Restart', function() {
          commonFunctions.restartFlow_Multi();
        });
    });
