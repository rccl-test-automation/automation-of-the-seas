var helper = require('../../../helper.js');
var CommonFunctions = require('../../common/common.functions.js');
var CommonElements = require('../../common/common.objects.js');
var SpaFunctions = require('../../common/SPA.functions.js');
var SpaElements = require('../../common/SPA.objects.js');
var LegacyElements = require('../../common/Legacy.objects.js');
var LegacyFunctions = require('../../common/Legacy.functions.js');
var MultiElements = require('../../common/Multi.objects.js');
var MultiFunctions = require('../../common/Multi.functions.js');

var spaFunctions = new SpaFunctions();
var spaElements = new SpaElements();
var commonFunctions = new CommonFunctions();
var commonElements = new CommonElements();
var legacyElements = new LegacyElements();
var legacyFunctions = new LegacyFunctions();
var multiElements = new MultiElements();
var multiFunctions = new MultiFunctions();
// //------------------------------------------------------------------------------------------------------------------------------------------------------------

  describe('AEM Pages: ', function() {
    beforeAll(function() {
      browser.get('browser.params.url');
      browser.ignoreSynchronization = true;
      browser.manage().deleteAllCookies();
      //eyes.open(browser, "Prod", "AEM Pages");
    });

    beforeEach(function() {
      browser.ignoreSynchronization = true;
    });
    afterEach(function() {
      browser.manage().deleteAllCookies();
    });
    afterAll(function() {
      //eyes.close();
      browser.manage().deleteAllCookies();
    });


    it('Ships Homepage', function() {

      spaFunctions.shipPage();
      //eyes.checkWindow("Ships Page");


    });
    it('Harmony', function() {
      spaFunctions.harmonyPage();
      //eyes.checkWindow("Harmony");
      spaFunctions.harmonyThings();
      //eyes.checkWindow("Harmony things-to-do");
      spaFunctions.harmonyDecks();
      //eyes.checkWindow("Harmony Deck Plans");
      spaFunctions.harmonyRooms();
      //eyes.checkWindow("Harmony Rooms");
      spaFunctions.harmonyAwards();
      //eyes.checkWindow("Harmony Awards");
    });

  });
