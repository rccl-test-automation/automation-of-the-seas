//var HtmlReporter = require('protractor-beautiful-reporter');
let SpecReporter = require('jasmine-spec-reporter').SpecReporter;

exports.config = {
  seleniumArgs: ['-browserTimeout=20012'],
  framework: 'jasmine',
  getPageTimeout: 4000000,
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['./Environments/USA/Prod.BookingFlow.js'],
  jasmineNodeOpts: {
    isVerbose: false,
    // If true, print colors to the terminal.
    showColors: true,
    // If true, include stack traces in failures.
    includeStackTrace: true,
    // Default time to wait in ms before a test fails.
    defaultTimeoutInterval: 4000000,
    silent: true,
    print: function() {}
  },
  capabilities: {
    'browserName': 'chrome',
    //directConnect: true,
    //Headless Mode
    //"--disable-gpu", "--headless", "--window-size=1782, 1102"
    //"--incognito", "--start-fullscreen"
    chromeOptions: {
      args: [
        "--disable-gpu", "--headless", "--window-size=1920, 1200"
        //"--incognito", "--start-fullscreen"
     ]
    },
    //'resolution': '1920x1200',
  },

  params: {
    url: 'https://www.royalcaribbean.com',
  },

  suites: {
    Prod: 'Environments/TestSuites/Prod/Prod.*.js',
    PreProd: 'Environments/USA/Preprod/Preprod.*.js',
    Stage2: 'Environments/USA/Stage2/Stage2.*.js',
  },

  // Setup the report before any tests start
  onPrepare: function() {

    var jasmineReporters = require('jasmine-reporters');
    var fs = require('fs-extra');

fs.emptyDir('screenshots/', function (err) {
        console.log(err);
    });

    jasmine.getEnv().addReporter({
        specDone: function(result) {
            if (result.status == 'failed') {
                browser.getCapabilities().then(function (caps) {
                    var browserName = caps.get('browserName');

                    browser.takeScreenshot().then(function (png) {
                        var stream = fs.createWriteStream('screenshots/' + browserName + '-' + result.fullName+ '.png');
                        stream.write(new Buffer(png, 'base64'));
                        stream.end();
                    });
                });
            }
        }
    });

    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
        consolidateAll: true,
        savePath: './',
        filePrefix: 'xmlresults'
    }));

    jasmine.getEnv().addReporter(new SpecReporter({
      spec: {
        displayStacktrace: true
      },
      summary: {
        displayDuration: true
      }
    }));

    browser.ignoreSynchronization = true;
    //browser.driver.manage().window().setSize(1782, 1102);

    var AllureReporter = require('jasmine-allure-reporter');
    jasmine.getEnv().addReporter(new AllureReporter({
      resultsDir: 'allure-results'
    }));
    var AllureReporter = require('jasmine-allure-reporter');
    jasmine.getEnv().addReporter(new AllureReporter());
    jasmine.getEnv().afterEach(function(done) {
      browser.takeScreenshot().then(function(png) {
        allure.createAttachment('Screenshot', function() {
          return new Buffer(png, 'base64')
        }, 'image/png')();
        done();
      })
    });
  },

  onComplete: function() {
     var browserName, browserVersion;
     var capsPromise = browser.getCapabilities();

     capsPromise.then(function (caps) {
        browserName = caps.get('browserName');
        browserVersion = caps.get('version');

        var HTMLReport = require('protractor-html-reporter');
        //var jasmineReporters = require('jasmine-reporters');

      // jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
      //     consolidateAll: true,
      //     savePath: './',
      //     filePrefix: 'xmlresults'
      // }));

        testConfig = {
            reportTitle: 'Test Report',
            outputPath: './',
            screenshotPath: './screenshots',
            testBrowser: browserName,
            browserVersion: browserVersion,
            modifiedSuiteName: true,
            screenshotsOnlyOnFailure: true,
            launchReport: true
        };
        new HTMLReport().from('xmlresults.xml', testConfig);
    });
 }

}
