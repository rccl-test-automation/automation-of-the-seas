# CucumberJS + Protractor + Chai

## Setup
1. Setup a Bitbucket Account
    * https://bitbucket.org/
2. Install Git on your Workstation
    * https://git-scm.com/download/mac
3. Install Visual Studio Code
    * https://code.visualstudio.com/
4. Clone the Project to Your Workstation
    * git clone
5. Install Node
    * https://nodejs.org/en/
6. Install NPM Modules:

**Webdriver Manager:** Sets up your selenium server and manages the associated browser binaries
```
npm install -g webdriver-manager
```
**Protractor:** A framework used to simplify the use of selenium and to better integrate with Angular Apps; although it can be used with any web application.
```
npm install protractor -g
```
**Cucumber Protractor Framework:** This module serves to integrate the protractor and cucumberjs framework. 
```
npm install --save-dev protractor-cucumber-framework
```
**Chai as Promised:** This is the assertion framework that allows you to pass or fail specific expectaions. 
```
npm install --save-dev chai-as-promised
```
**Reporting Plugin:** To roll up pass, fail, screenshots, and error results by feature in a pretty dashboard.  
``` 
npm install -g protractor-multiple-cucumber-html-reporter-plugin
```
### Dependencies
- Node 8.9.4 LTS